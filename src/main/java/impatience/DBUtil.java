package impatience;

import java.util.*;

/**Helps you deal with databases.*/
public class DBUtil
{
	/**Separates a SQL script into individual commands.
	 * This function shouldn't choke if you include extra semicolons in comments (--line and /*...* /).
	 * This function WILL choke if you don't put a semicolon after every single statement.
	 * (Treat semicolons as terminators, not separators.)*/
	public static List<String> splitScript(String script)
	{
		List<String> out = new ArrayList<>();

		int start = 0, end = 0;//a subset [start, end) of the script: the current statement
		boolean inDashComment = false, inSlashComment = false;

		for( ; end < script.length(); end++)
		{
			char current = script.charAt(end);
			//if we've found a non-comment semicolon, execute this statement
			if(current == ';' && !inDashComment && !inSlashComment)
			{
				out.add(script.substring(start, end));
				start = end + 1;
			}
			//if we've found the start of a -- comment
			else if(current == '-' && !inDashComment && !inSlashComment && end + 1 < script.length() && script.charAt(end + 1) == '-')
				inDashComment = true;
				//if we've found the start of a /* comment
			else if(current == '/' && !inDashComment && !inSlashComment && end + 1 < script.length() && script.charAt(end + 1) == '*')
				inSlashComment = true;
				//if we've found the end of a -- comment
			else if(current == '\n' && inDashComment)
				inDashComment = false;
				//if we've found the end of a */ comment
			else if(current == '*' && inSlashComment && end + 1 < script.length() && script.charAt(end + 1) == '/')
				inSlashComment = false;
		}

		return out;
	}
}
