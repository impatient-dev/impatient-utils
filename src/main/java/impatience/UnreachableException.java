package impatience;

/**An exception to use in areas that it should not be possible to reach,
but where the compiler requires something because it cannot determine this.*/
@Deprecated public class UnreachableException extends RuntimeException
{
	public UnreachableException(){super("It should not be possible to reach this code.");}
}
