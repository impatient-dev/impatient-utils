package impatience;

import java.util.concurrent.TimeUnit;

/**Wrappers around Java threading APIs that suppress InterruptedExceptions. (They are not rethrown.)*/
public class Threading
{
	private static final long MILLIS_TO_NANOS = TimeUnit.MILLISECONDS.toNanos(1);

	public static void sleep(long millis)
	{
		try{Thread.sleep(millis);}
		catch(InterruptedException e){/*ignore*/}
	}

	/**Calls sleep with a duration provided in nanoseconds.*/
	public static void sleepNanos(long nanos)
	{
		long millis = nanos / MILLIS_TO_NANOS;
		int remNanos = (int)(nanos % MILLIS_TO_NANOS);
		try{Thread.sleep(millis, remNanos);}
		catch(InterruptedException e){/*ignore*/}
	}


	/**Calls wait on an object and suppresses any InterruptedException.
	 * You still have to acquire a lock on the object yourself.*/
	@SuppressWarnings({"WaitNotifyWhileNotSynced"})
	public static void wait(Object obj)
	{
		try{obj.wait();}
		catch(InterruptedException e){/*ignore*/}
	}

	/**Calls wait on an object and suppresses any InterruptedException.
	 * You still have to acquire a lock on the object yourself.*/
	@SuppressWarnings({"WaitNotifyWhileNotSynced"})
	public static void wait(Object obj, long ms)
	{
		try{obj.wait(ms);}
		catch(InterruptedException e){/*ignore*/}
	}
}
