package impatience;

import java.util.*;

public class SqlUtil
{
	/**Takes a SQL script (1 or more SQL statements, terminated by semicolons)
	 * and separates it into an array of individual statements.
	 * WARNING: current implementation just looks for semicolons;
	 * if you need to handle comments, etc, improve this function.*/
	public static String[] splitScript(String script)
	{
		List<String> out = new ArrayList<>();
		int start = 0;

		for(int end = 0; end < script.length(); end++)
		{
			char cur = script.charAt(end);
			if(cur == ';')
			{
				out.add(script.substring(start, end));
				start = end + 1;
			}
		}

		return out.toArray(new String[out.size()]);
	}
}
