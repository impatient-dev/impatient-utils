package impatience;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Locale;

import static java.util.Calendar.*;

/**Formats text.*/
public class TextFmt
{
	private static final NumberFormat format2 = new DecimalFormat("00"),
			format4 = new DecimalFormat("0000");


	/**Turns a date into a string YYYYMMDD.*/
	public static String dateCompact(int year, int month, int day)
	{
		return format4.format(year) + format2.format(month) + format2.format(day);
	}

	/**Like 2010 Jan 1 (Wed).*/
	public static String dateDetailedShort(Calendar cal)
	{
		return cal.get(YEAR) + " " +
				cal.getDisplayName(MONTH, SHORT, Locale.getDefault()) + " " +
				cal.get(DATE) + " (" +
				cal.getDisplayName(DAY_OF_WEEK, SHORT, Locale.getDefault()) + ")";
	}

	/**Turns the number of seconds since midnight into a 24-hour time h:mm (without seconds).*/
	public static String time24(int seconds)
	{
		int hour = seconds / 3600, minute = (seconds % 3600) / 60;
		return hour + ":" + format2.format(minute);
	}

	/**Turns the number of seconds since midnight into a 24-hour time that is always 5 characters long (hh:mm).*/
	public static String time24Full(int seconds)
	{
		int hour = seconds / 3600, minute = (seconds % 3600) / 60;
		return format2.format(hour) + ":" + format2.format(minute);
	}

	public static String time24(Calendar cal){return cal.get(Calendar.HOUR_OF_DAY) + ":" + format2.format(cal.get(Calendar.MINUTE));}
}
