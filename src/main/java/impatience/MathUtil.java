package impatience;

import static java.lang.Math.*;

//TODO make sure int, long, float, double all have their own variant, where reasonable
public class MathUtil
{
	/**Returns the absolute value of the difference between a and b in a mod-m world.
	 * So 1 and 90 are actually quite close if m=100.*/
	public static float absModDiff(float a, float b, float m)
	{
		a = pmod(a, m);
		b = pmod(b, m);
		float larger, smaller;
		if(a > b)
		{
			larger = a;
			smaller = b;
		}
		else
		{
			larger = b;
			smaller = a;
		}
		return min(larger - smaller, smaller - larger + m);
	}



	/**Positive modulus.
	 * Returns a mod b, picking the smallest positive result possible.*/
	public static double pmod(double a, double b)
	{
		double out = a % b;
		if(out < 0)
			out += b;
		return out;
	}
	/**Positive modulus.
	 * Returns a mod b, picking the smallest positive result possible.*/
	public static float pmod(float a, float b)
	{
		float out = a % b;
		if(out < 0)
			out += b;
		return out;
	}
	/**Positive modulus.
	 * Returns a mod b, picking the smallest positive result possible.*/
	public static int pmod(int a, int b)
	{
		int out = a % b;
		if(out < 0)
			out += b;
		return out;
	}
	/**Positive modulus.
	 * Returns a mod b, picking the smallest positive result possible.*/
	public static long pmod(long a, long b)
	{
		long out = a % b;
		if(out < 0)
			out += b;
		return out;
	}



	public static int roundDown(double d){return (int)Math.floor(d);}

	/**Rounds a to the nearest multiple of b.*/
	public static int roundToMultiple(int a, int b)
	{
		return (int)Math.round(a / (double)b) * b;
	}



	/**Computes powers of 2.*/
	public static int pow2(int pow)
	{
		return 1 << pow;
	}
}