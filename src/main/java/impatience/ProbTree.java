package impatience;

import javax.annotation.Nullable;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**Class that lets you pick from a largish number of choices according to probabilistic weights.*/
public class ProbTree<T>
{
	@Nullable Node root = null;


	public void add(T value, int weight)
	{
		if(weight == 0)
			return;
		if(weight < 0)
			throw new IllegalArgumentException("negative weights not allowed: " + weight);

		if(root == null)
			root = new Node(value, weight);
		else
			addHelper(root, value, weight);
	}


	/**Tries to add a node to the provided parent, but recurses if necessary.*/
	private void addHelper(Node parent, T value, int weight)
	{
		if(parent.left == null)
			parent.left = new Node(value, weight);
		else if(parent.right == null)
			parent.right = new Node(value, weight);
		else
		{
			Node next = ThreadLocalRandom.current().nextInt(parent.left.weightSum + parent.right.weightSum) < parent.left.weightSum ? parent.left : parent.right;
			addHelper(next, value, weight);
		}
		parent.weightSum += weight;
	}



	/**Picks a random value according to the relative weights of all added values.*/
	public T pickRand(Random rand)
	{
		if(root == null)
			throw new IllegalStateException();

		Node current = root;
		while(true)
		{
			int left = current.left == null ? 0 : current.left.weightSum;
			int right = current.right == null ? 0 : current.right.weightSum;
			int center = current.weight;
			int r = rand.nextInt(left + right + center);
			if((r -= center) < 0)
				return current.value;
			else if((r -= left) < 0)
				current = current.left;
			else
				current = current.right;
		}
	}



	private class Node
	{
		public final T value;
		public final int weight;
		/**Sum of this node's weight and all the weights below.*/
		public int weightSum;
		@Nullable Node left = null, right = null;

		public Node(T value, int weight)
		{
			this.value = value;
			this.weight = weight;
			this.weightSum = weight;
		}
	}
}
