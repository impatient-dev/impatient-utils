package impatience.math;

import javax.annotation.Nullable;

/**Functions dealing with geometry.
 * All functions return new objects that are independent of the input parameters.
 * No functions modify their arguments.*/
public class GeomUtil
{
	/**Returns dx^2/rx^2 + dy^2/ry^2.
	 * A point is within an ellipse if this is less than 1 1.*/
	public static double ellipseFarness(double pointX, double pointY, double ellipseX, double ellipseY, double radiusX, double radiusY)
	{
		double dx = pointX - ellipseX, dy = pointY - ellipseY;
		return dx * dx / radiusX / radiusX + dy * dy / radiusY / radiusY;
	}

	/**Returns whether or not an axis-alligned ellipse contains a point.
	 * Returns true for the boundary case.*/
	public static boolean ellipseContains(double pointX, double pointY, double ellipseX, double ellipseY, double radiusX, double radiusY)
	{
		return ellipseFarness(pointX, pointY, ellipseX, ellipseY, radiusX, radiusY) <= 1;
	}


	/**Finds the intersection of 2 lines, if any.
	 * Returns null for parallel lines, regardless of whether there is any intersection.*/
	@Nullable public static Vec2 intersectionLines(Vec2 point1, Vec2 slope1, Vec2 point2, Vec2 slope2)
	{
		slope1 = slope1.toUnit();
		slope2 = slope2.toUnit();

		//check for parallel lines
		if(slope1.equals(slope2, 0.000001))
			return null;

		//let point 1 be translated to the origin; calculate the new point2
		Vec2 point2Trans = point2.minus(point1);

		//rotate everything so line 1 is the x-axis
		double angle = slope1.angle();
		Vec2 relp2 = point2Trans.rotated(-angle), rels2 = slope2.rotated(-angle);

		//find where the transformed line 2 hits the x-axis
		double t = - relp2.y / rels2.y;//how far you have to 'go along' the slope to get to y=0
		double relX = relp2.x + t * rels2.x;
		Vec2 relResult = new Vec2(relX, 0);

		//transform the result back into the original coordinate system
		Vec2 unrotatedResult = relResult.rotated(angle);
		return unrotatedResult.plus(point1);
	}

	/**Finds the intersection of 2 line segments, if any.*/
	@Nullable public static Vec2 intersectionSegments(Vec2 seg1a, Vec2 seg1b, Vec2 seg2a, Vec2 seg2b)
	{
		//get the intersection of the 2 lines
		Vec2 slope1 = seg1b.minus(seg1a), slope2 = seg2b.minus(seg2a);
		Vec2 intersection = intersectionLines(seg1a, slope1, seg2a, slope2);
		if(intersection == null)
			return null;

		//ensure the result is on both segments
		double t = paramProjectOntoLine(intersection, seg1a, seg1b);
		if(t < 0 || t > 1)
			return null;
		t = paramProjectOntoLine(intersection, seg2a, seg2b);
		if(t < 0 || t > 1)
			return null;
		return intersection;
	}



	/**Finds the projection of a point onto a line segment.*/
	public static Vec2 projectionOntoSegment(Vec2 point, Vec2 segmentStart, Vec2 segmentEnd)
	{
		double param = paramProjectOntoLine(point, segmentStart, segmentEnd);
		if(param <= 0)
			return segmentStart.clone();
		if(param >= 1)
			return segmentEnd.clone();
		return segmentStart.plus(segmentEnd.minus(segmentStart).times(param));
	}

	/**Finds the projection of a point onto a line that goes through points a and b.*/
	public static Vec2 projectionOntoLine(Vec2 point, Vec2 a, Vec2 b)
	{
		Vec2 slope = b.minus(a);
		Vec2 relativePoint = point.minus(a);
		double param = paramProjectOntoLine(relativePoint, slope);
		return a.plus(slope.times(param));
	}

	/**Finds the orthogonal projection of a point onto the line that goes through A and B.
	 * Returns the parameter T such that the projection is at A + T (B - A).*/
	public static double paramProjectOntoLine(Vec2 point, Vec2 a, Vec2 b)
	{
		Vec2 slope = b.minus(a);
		Vec2 relativePoint = point.minus(a);
		return paramProjectOntoLine(relativePoint, slope);
	}
	/**Finds the orthogonal projection of a point onto a line that goes through the origin and has the provided slope.
	 * Returns the parameter T such that the projection is at T * slope.*/
	public static double paramProjectOntoLine(Vec2 point, Vec2 lineSlope){return (point.dot(lineSlope) / lineSlope.dot(lineSlope));}
}
