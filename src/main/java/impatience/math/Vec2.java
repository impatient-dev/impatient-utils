package impatience.math;

/**A 2D vector/point. TODO replace with the kotlin version in impatient-slippy*/
public class Vec2 implements Cloneable
{
	public double x, y;

	public Vec2(double x, double y)
	{
		this.x = x;
		this.y = y;
	}


	@Override public String toString(){return "(" + x + ", " + y + ")";}

	/**Converts polar coordinates into a (Cartesian) vector.*/
	public static Vec2 polar(double angle, double magnitude){return new Vec2(magnitude * Math.cos(angle), magnitude * Math.sin(angle));}


	@Override public Vec2 clone(){return new Vec2(x,y);}
	@Override public boolean equals(Object obj)
	{
		return obj instanceof Vec2 && equals((Vec2)obj);
	}
	public boolean equals(Vec2 that){return x == that.x && y == that.y;}
	public boolean equals(Vec2 that, double tolerance)
	{
		return Math.abs(x - that.x) <= tolerance && Math.abs(y - that.y) <= tolerance;
	}
	public void set(Vec2 src)
	{
		this.x = src.x;
		this.y = src.y;
	}


	/**Returns the magnitude of this vector.
	 * Since this involves a square root, it is slower than magnitude2().*/
	public double magnitude(){return Math.sqrt(magnitude2());}
	/**Returns the square of the magnitude of this vector.*/
	public double magnitude2(){return x * x + y * y;}
	/**Returns the larger of abs(x) and abs(y).*/
	public double maxAbsComponent()
	{
		double xx = Math.abs(x), yy = Math.abs(y);
		return xx >= yy ? xx : yy;
	}

	/**Returns the angle of this vector.
	 * Uses the math convention (radians counter-clockwise from the +X axis).*/
	public double angle(){return Math.atan2(y, x);}

	public Vec2 plus(Vec2 v){return new Vec2(x + v.x, y + v.y);}
	public Vec2 minus(Vec2 v){return new Vec2(x - v.x, y - v.y);}
	public Vec2 times(double scalar){return new Vec2(scalar * x, scalar * y);}
	public double dot(Vec2 v){return x * v.x + y * v.y;}

	/**Rotates this vector by the provided angle (radians) counter-clockwise.*/
	public Vec2 rotated(double angle){return polar(angle() + angle, magnitude());}
	/**Returns a unit vector with the same direction as this one.*/
	public Vec2 toUnit()
	{
		double magnitude = magnitude();
		return new Vec2(x / magnitude, y / magnitude);
	}


	/**Returns the square of the distance to another point.*/
	public double distance2(Vec2 that)
	{
		double dx = x - that.x, dy = y - that.y;
		return dx * dx + dy * dy;
	}
	/**Returns the distance to another point.*/
	public double distance(Vec2 that){return Math.sqrt(distance2(that));}
}
