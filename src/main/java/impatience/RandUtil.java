package impatience;

import java.util.List;
import java.util.Random;

/**Helper functions for random numbers.*/
public class RandUtil
{
	/**Returns a random item from the array.*/
	public static <T> T randEntry(T[] arr, Random rand)
	{
		return arr[rand.nextInt(arr.length)];
	}
	/**Returns a random item from the list.*/
	public static <T> T randEntry(List<T> list, Random rand)
	{
		return list.get(rand.nextInt(list.size()));
	}

	/**Returns an integer within the provided range (inclusive).*/
	public static int nextInt(int min, int max, Random rand)
	{
		return min + rand.nextInt(max - min + 1);
	}
}
