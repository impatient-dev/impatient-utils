package impatience;

import java.util.Iterator;

public class TextUtil
{
	/**Joins several parts into a string.*/
	public static String join(String[] parts, String separator)
	{
		if(parts.length == 0)
			return "";
		StringBuilder out = new StringBuilder(parts[0]);
		for(int c = 1; c < parts.length; c++)
		{
			out.append(separator);
			out.append(parts[c]);
		}
		return out.toString();
	}

	/**Joins several parts into a string.*/
	public static String join(Iterable<String> parts, String separator)
	{
		StringBuilder build = new StringBuilder();
		Iterator<String> it = parts.iterator();
		if(!it.hasNext())
			return "";
		while(true)
		{
			build.append(it.next());
			if(it.hasNext())
				build.append(separator);
			else
				break;
		}
		return build.toString();
	}

	/**Returns a string like "1st", "2nd", "3rd", "4th", etc.*/
	public static String ordinalNumber(int n)
	{
		switch(n % 10)
		{
			case 1: return n + "st";
			case 2: return n + "nd";
			case 3 : return n + "rd";
			default: return n + "th";
		}
	}
}
