package impatience;

/**A property that can be set and unsed and that has a default value.*/
public class Improperty<T>
{
	/**The value of this property to use when it is unset.*/
	public final T defaultValue;
	private T value;
	private boolean isSet;


	/**Creates a property that is initially unset and where the default value is null.*/
	public Improperty(){this(null);}

	/**Creates a property that is initially unset but has a default value.*/
	public Improperty(T defaultValue)
	{
		this.defaultValue = defaultValue;
		isSet = false;
	}

	/**Creates a property that is initially set but still has a default value.*/
	public Improperty(T defaultValue, T initialValue)
	{
		this.defaultValue = defaultValue;
		this.value = initialValue;
		isSet = true;
	}



	/**Returns the value if set, or the default if unset.*/
	public T get(){return isSet ? value : defaultValue;}
	/**Returns whether or not the property is set.*/
	public boolean isSet(){return isSet;}
	/**Sets the property.*/
	public void set(T value){this.value = value;}
	/**Unsets the property so the default value will be used.*/
	public void reset(){isSet = false;}
}
