package imp.util

import java.net.URL


fun URL.withoutProtocol(): String {
	val s = toString()
	var i = protocol.length
	if(s.contains("://"))
		i += 3
	return s.substring(i)
}