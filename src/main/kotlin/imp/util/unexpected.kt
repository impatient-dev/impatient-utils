package imp.util


/**Throws an exception. Call this when you encounter something unexpected and need to crash.*/
fun unexpected(thing: Any?): Nothing {
	throw RuntimeException("Unexpected: $thing")
}

/**Throws an exception.*/
fun unreachable(): Nothing {
	throw RuntimeException("This code should be unreachable")
}
fun unreachable(msg: String): Nothing {
	throw RuntimeException("This situation should not be reachable: $msg")
}