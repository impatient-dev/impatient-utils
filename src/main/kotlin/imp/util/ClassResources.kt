package imp.util

import org.apache.commons.io.IOUtils
import java.io.InputStream
import kotlin.reflect.KClass

object ClassResources {
	private val log = ClassResources::class.logger

	/**Opens a classpath resource (probably something in src/main/resources) using some class in your module and a path.
	 * The path should be absolute, starting with "/".*/
	fun open(cls: KClass<*>, path: String): InputStream {
		log.debug("Loading classpath resource {} from {}.", path, cls.java.name)
		return cls.java.getResourceAsStream(path)!!
	}

	/**Opens a classpath resource (probably something in src/main/resources) using some class in your module and a path.
	 * The path should be absolute, starting with "/".*/
	fun loadAsString(cls: KClass<*>, path: String): String {
		return IOUtils.toString(open(cls, path), Charsets.UTF_8)
	}
}