package imp.util

import java.util.*
import java.util.concurrent.ThreadLocalRandom

fun <T> Random.next(array: Array<T>): T = array[nextInt(array.size)]
fun <T> Random.next(list: List<T>): T = list[nextInt(list.size)]


/**Generates a normally distributed value.*/
fun Random.nextGaussian(mean: Double, standardDeviation: Double): Double = this.nextGaussian() * standardDeviation + mean

/**The probability should be in [0-1]. If the probability is 0.75, this function returns true about 75% of the time.*/
fun Random.nextProb(probability: Double): Boolean = this.nextDouble() < probability
/**The probability should be in [0-1]. If the probability is 0.75, this function returns true about 75% of the time.*/
fun Random.nextProb(probability: Float): Boolean = this.nextFloat() < probability



fun <T> MutableList<T>.shuffleSublist(startInclusive: Int, endExclusive: Int, rand: ThreadLocalRandom = ThreadLocalRandom.current()) {
	check(startInclusive <= endExclusive) { "start=$startInclusive, end=$endExclusive" }
	for(i in 0 until endExclusive - 1)
		Collections.swap(this, i, rand.nextInt(i, endExclusive - 1))
}

/**For a sorted list (either ascending or descending), randomly reorders ties (entries where the comparison returns 0).*/
inline fun <T> MutableList<T>.sortedPermuteTies(
	compare: (T,T) -> Int,
) {
	val rand = ThreadLocalRandom.current()
	var start = 0
	while(start < lastIndex - 1) {
		var end = start
		while(end < lastIndex && compare(this[start], this[end + 1]) == 0)
			end++
		if(start != end)
			shuffleSublist(start, end + 1, rand)
		start = end + 1
	}
}