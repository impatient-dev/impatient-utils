package imp.util

import org.apache.commons.io.FileUtils
import java.io.File
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.util.zip.ZipFile

/**Unzips a file to a directory, deleting the directory if it exists.*/
fun unzip(zipFile: Path, outDir: Path) {
	if(Files.exists(outDir))
		FileUtils.forceDelete(outDir.toFile())

	ZipFile(zipFile.toFile()).use {zin ->
		zin.stream().use {entries ->
			entries.forEach {entry ->
				if(entry.isDirectory)
					Files.createDirectories(Paths.get(outDir.toString(), entry.name))
				else {
					zin.getInputStream(entry).use {ins ->
						val outFile = File(outDir.toFile(), entry.name)
						FileUtils.copyToFile(ins, outFile)
					}
				}
			}
		}
	}
}