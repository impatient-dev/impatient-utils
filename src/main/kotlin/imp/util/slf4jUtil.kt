package imp.util

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import kotlin.reflect.KClass

/**Gets a SLF4J logger from a Kotlin class.*/
val KClass<*>.logger: Logger get() = LoggerFactory.getLogger(this.java)
fun logger(name: String): Logger = LoggerFactory.getLogger(name)


/**Convenience method that logs the result of a Kotlin lambda correctly.*/
inline fun Logger.kerror(message: () -> String) {
	if(this.isErrorEnabled)
		this.error(message())
}
/**Convenience method that logs the result of a Kotlin lambda correctly.*/
inline fun Logger.kwarn(message: () -> String) {
	if(this.isWarnEnabled)
		this.warn(message())
}
/**Convenience method that logs the result of a Kotlin lambda correctly.*/
inline fun Logger.kinfo(message: () -> String) {
	if(this.isInfoEnabled)
		this.info(message())
}
/**Convenience method that logs the result of a Kotlin lambda correctly.*/
inline fun Logger.kdebug(message: () -> String) {
	if(this.isDebugEnabled)
		this.debug(message())
}
/**Convenience method that logs the result of a Kotlin lambda correctly.*/
inline fun Logger.ktrace(message: () -> String) {
	if(this.isTraceEnabled)
		this.trace(message())
}