package imp.util

import java.time.*

fun Long.epochMsToInstant() = Instant.ofEpochMilli(this)

fun localDateTimeFromEpochMilli(millis: Long, zoneId: ZoneId = ZoneId.systemDefault()): LocalDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(millis), zoneId)

fun Instant.toLocal(zoneId: ZoneId = ZoneId.systemDefault()): LocalDateTime = LocalDateTime.ofInstant(this, zoneId)

fun LocalDateTime.toInstant(zone: ZoneId = ZoneId.systemDefault()) = atZone(zone).toInstant()

fun LocalDateTime.withDate(year: Int, month: Int, day: Int) = withYear(year).withMonth(month).withDayOfMonth(day)
fun LocalDateTime.withTime(hour: Int, minute: Int = 0, second: Int = 0) = this.withHour(hour).withMinute(minute).withSecond(second)

fun ZonedDateTime.withDate(year: Int, month: Int, day: Int) = withYear(year).withMonth(month).withDayOfMonth(day)
fun ZonedDateTime.withTime(hour: Int, minute: Int = 0, second: Int = 0) = this.withHour(hour).withMinute(minute).withSecond(second)

fun LocalDate.yyyymmdd(): Int = (year * 1_00_00) + (monthValue * 100) + dayOfMonth