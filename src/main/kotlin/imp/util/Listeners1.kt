package imp.util

/**A collection of listeners that can be added to and removed from. Not thread-safe - the "1" means single-threaded.*/
interface Listeners1 <T> {
	fun add(listener: T)
	fun remove(listener: T)
}



/**A basic implementation that stores a list of listeners. This will get slow if you have a large number of listeners that are added/removed frequently.*/
class BasicListeners1 <T> : Listeners1<T> {
	private val listeners = ArrayList<T>()

	override fun add(listener: T) {
		listeners.add(listener)
	}

	override fun remove(listener: T) {
		listeners.remove(listener)
	}

	/**Calls the block for each listener, synchronously.*/
	fun fire(block: (listener: T) -> Unit) {
		for(listener in listeners)
			block(listener)
	}
}