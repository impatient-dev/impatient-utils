package imp.util


/**Contains a list plus a number of elements that were removed/truncated.*/
class PossiblyTruncatedList <T> (
	/**The first few entries.*/
	val entries: List<T>,
	/**The total number of entries were in the list before truncation.*/
	val total: Int,
) {
	inline val size get() = entries.size
	/**The number of entries that were truncated from the list.*/
	val missing: Int get() = total - entries.size
	val isTruncated: Boolean get() = missing > 0

	companion object {
		fun <T> empty() = PossiblyTruncatedList<T>(emptyList(), 0)

		/**Returns an object containing the whole list, or containing only the first `limit` entries if it's too long.*/
		fun <T> of(limit: Int, list: List<T>): PossiblyTruncatedList<T> {
			if (list.size > limit) {
				val truncated = list.subList(0, limit)
				return PossiblyTruncatedList(truncated, list.size)
			}
			return PossiblyTruncatedList(list, list.size)
		}

		fun <T> of(limit: Int, vararg items: T): PossiblyTruncatedList<T> = of(limit, items.toList())
	}

	/**Returns a list with 1 fewer item. The total is decremented as well.*/
	fun minusAt(index: Int): PossiblyTruncatedList<T> = PossiblyTruncatedList(entries.minusAt(index), total - 1)
}

fun <T> List<T>.possiblyTruncated(limit: Int) = PossiblyTruncatedList.of(limit, this)