package imp.util

import kotlin.math.PI
import kotlin.math.ceil
import kotlin.math.floor
import kotlin.math.sqrt

/**Returns the value, unless it's outside the range [min, max], in which case min or max is returned.*/
fun clamp(min: Int, value: Int, max: Int): Int =
	if(value < min) min
	else if(value > max) max
	else value
/**Returns the value, unless it's outside the range [min, max], in which case min or max is returned.*/
fun clamp(min: Float, value: Float, max: Float): Float =
	if(value < min) min
	else if(value > max) max
	else value
/**Returns the value, unless it's outside the range [min, max], in which case min or max is returned.*/
fun clamp(min: Double, value: Double, max: Double): Double =
	if(value < min) min
	else if(value > max) max
	else value

fun pow(n: Int, power: Int): Int {
	if(power < 0)
		throw IllegalArgumentException("power: $power")
	var out = 1
	for(i in 0 until power)
		out *= n
	return out
}

fun pow(n: Long, power: Long): Long {
	if(power < 0)
		throw IllegalArgumentException("power: $power")
	var out = 1L
	for(i in 0 until power)
		out *= n
	return out
}

/**Returns null if this is NaN or infinite.*/
inline fun Float.finite() = if(this.isFinite()) this else null
/**Returns null if this is NaN or infinite.*/
inline fun Double.finite() = if(this.isFinite()) this else null

inline fun Float.sqrt() = sqrt(this)
inline fun Double.sqrt() = sqrt(this)

inline fun Float.squared() = this * this
inline fun Double.squared() = this * this


inline fun Double.roundUp(): Int = ceil(this).toInt()
inline fun Double.roundDown(): Int = floor(this).toInt()

inline fun Float.roundUp(): Int = ceil(this).toInt()
inline fun Float.roundDown(): Int = floor(this).toInt()

/**Returns a / b, rounding up.*/
fun divRoundUp(a: Long, b: Long): Long = a / b + if(a % b == 0L) 0 else 1


/**Converts radians to degrees.*/
fun Float.degrees(): Float = (this / PI * 180).toFloat()
/**Converts degrees to radians.*/
fun Float.radians(): Float = (this / 180 * PI).toFloat()
/**Converts radians to degrees. The result will be in [0, 360).*/
fun Float.degrees0360(): Float {
	var out = this.degrees() % 360
	if(out < 0) out += 360
	return out
}

/**Converts radians to degrees.*/
fun Double.degrees(): Double = this / PI * 180
/**Converts degrees to radians.*/
fun Double.radians(): Double = this / 180 * PI

/**Converts degrees, minutes, and seconds to degrees.*/
fun degreesMinSec(degrees: Double, minutes: Double = 0.0, seconds: Double = 0.0): Double =
	(seconds / 60 + minutes) / 60 + degrees


/**Throws instead of overflowing.*/
inline fun Long.toIntExact() = Math.toIntExact(this)