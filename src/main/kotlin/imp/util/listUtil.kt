package imp.util


inline fun <T> MutableList<T>.replaceAt(index: Int, transform: (T) -> T) {
	this[index] = transform(this[index])
}

fun <T> Collection<T>.toArrayListWithExtraCapacity(extra: Int): ArrayList<T> {
	val out = ArrayList<T>(this.size + extra)
	out.addAll(this)
	return out
}

/**Starting at the end of the list, checks if each item matches the condition, stopping the first time a check returns false.
 * Returns the "last" visited index that passes the check, i.e. the one with the lowest index, closest to the start of the list.
 * Returns -1 if the item at the list doesn't pass the check, or if the list is empty.*/
inline fun <T> List<T>.lastIndexFromLast(condition: (T) -> Boolean): Int {
	if(isEmpty() || !condition(last()))
		return -1
	var out = lastIndex
	while(out > 0) {
		if(!condition(this[out - 1]))
			break
		out--
	}
	return out
}


/**Removes all items with indexes >= the provided size.*/
fun <T> MutableList<T>.cutDownToSize(desiredSize: Int) {
	while(size > desiredSize)
		this.removeLast()
}
/**Removes every item with an index higher than the provided one.*/
fun <T> MutableList<T>.removeEverythingAfter(index: Int) = cutDownToSize(index + 1)