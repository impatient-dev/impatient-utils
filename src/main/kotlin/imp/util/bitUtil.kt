package imp.util


/**Sets 4 bytes in the array, starting with the byte at the specified position.
 * This is a big-endian function; the pos + 0 byte will be set to the most significant byte of the value,
 * while the pos + 3 byte will be set to the least significant byte of the value.*/
fun ByteArray.putInt(pos: Int, value: Int) {
	this[pos] = (value shr 24).toByte()
	this[pos + 1] = (value shr 16).toByte()
	this[pos + 2] = (value shr 8).toByte()
	this[pos + 3] = value.toByte()
}

/**Reads an int out of 4 bytes in the array, starting with the byte at the specified position.
 * This is a big-endian function: the byte at pos + 0 will be the most significant one in the return value, while the one at pos + 3 will be the least significant.*/
fun ByteArray.getInt(pos: Int): Int {
	return (this[pos].asInt shl 24) or
		(this[pos + 1].asInt shl 16) or
		(this[pos + 2].asInt shl 8) or
		this[pos + 3].asInt
}


/**Sets the requested bits to 1 or 0.*/
inline fun Int.withBits(mask: Int, one: Boolean) = if(one) this or mask else this and mask.inv()


/**Generates a bitmask where the rightmost (least significant) bits are set to 1, and the others to 0.*/
fun intBitmaskR(bitCount: Int): Int {
	check(bitCount <= 32) { "bits=$bitCount" }
	var out = 0
	for(i in 0 until bitCount)
		out = (out shl 1) or 1
	return out
}
/**Generates a bitmask where the rightmost (least significant) bits are set to 1, and the others to 0.*/
fun longBitmaskR(bitCount: Int): Long {
	check(bitCount <= 64) { "bits=$bitCount" }
	var out = 0L
	for(i in 0 until bitCount)
		out = (out shl 1) or 1L
	return out
}


inline fun Int.bitwiseAnd(mask: Int): Boolean = (this and mask) != 0
inline fun Long.bitwiseAnd(mask: Long): Boolean = (this and mask) != 0L

/**Returns 1 or 0.*/
inline val Boolean.asBit: Int get() = if(this) 1 else 0

/**Returns 0 if the boolean is false; returns the original number otherwise.*/
inline fun Int.bitsOnlyIf(condition: Boolean): Int = if(condition) this else 0
/**Returns 0 if the boolean is false; returns the original number otherwise.*/
inline fun Long.bitsOnlyIf(condition: Boolean): Long = if(condition) this else 0L

inline infix fun Int.shlBytes(bits: Int) = this shl (bits * 8)
inline infix fun Long.shlBytes(bits: Int) = this shl (bits * 8)

inline infix fun Int.shrBytes(bits: Int) = this shr (bits * 8)
inline infix fun Long.shrBytes(bits: Int) = this shr (bits * 8)



fun Sequence<Int>.bitwiseOr(): Int {
	return this.reduceOrNull { a, b -> a or b } ?: 0
}
fun Sequence<Long>.bitwiseOr(): Long {
	return this.reduceOrNull { a, b -> a or b } ?: 0
}