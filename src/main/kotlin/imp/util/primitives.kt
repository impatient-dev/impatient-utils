package imp.util


/**Converts this byte to an Int. This function only sets the least significant 8 bits, whereas toInt() fills the rest with the sign of the byte.*/
inline val Byte.asInt: Int get() = this.toInt() and 0xFF



inline val Int.b: Byte get() = this.toByte()
inline val Int.s: Short get() = this.toShort()

inline val UInt.b: Byte get() = this.toByte()
inline val UInt.s: Short get() = this.toShort()

inline val Int.ub: UByte get() = this.toUByte()
inline val Int.us: UShort get() = this.toUShort()

inline val UInt.ub: UByte get() = this.toUByte()
inline val UInt.us: UShort get() = this.toUShort()

inline val Byte.u: UByte get() = this.toUByte()
inline val Short.u: UShort get() = this.toUShort()
inline val Int.u: UInt get() = this.toUInt()
inline val Long.u: ULong get() = this.toULong()