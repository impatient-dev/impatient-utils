package imp.util

import java.util.concurrent.ThreadFactory

private val log = ReusableNameThreadFactory::class.logger

/**A thread factory that assigns names to threads, and reuses the names when threads stop.
 * Not particularly performant; intended for creating threads occasionally, not frequently.*/
class ReusableNameThreadFactory (
	/**Thread names will be of the form baseName-1, etc.*/
	val baseName: String,
	val daemon: Boolean = true,
	/**The first thread will end with this number; subsequent threads will end with higher numbers.*/
	val firstIndex: Int = 1,
	/**If true, a thread named after the firstIndex will not include the index (e.g. "work-1" becomes "work").
	 * Use this when you normally have 1 thread, but want decent names if more threads are created.*/
	val hideFirstIndex: Boolean = false,
) : ThreadFactory {
	private val spawnedThreads = ArrayList<Thread?>()


	@Synchronized override fun newThread(runnable: Runnable): Thread {
		var index: Int? = null

		//clear dead threads, find a name to reuse
		for(i in 0 until spawnedThreads.size) {
			if(spawnedThreads[i] == null || spawnedThreads[i]!!.state == Thread.State.TERMINATED) {
				spawnedThreads[i] = null
				if(index == null)
					index = i
			}
		}

		if(index == null)
			index = spawnedThreads.size

		val name = if(index == firstIndex && hideFirstIndex) baseName else "$baseName-${index + firstIndex}"
		val thread = Thread(runnable, name)
		thread.isDaemon = daemon
		spawnedThreads.addOrSet(index, thread)
		return thread
	}
}