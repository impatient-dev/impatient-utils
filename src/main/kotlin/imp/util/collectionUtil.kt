package imp.util

import java.util.stream.Collectors
import java.util.stream.Stream


fun <T> Stream<T>.collectToArrayList(): ArrayList<T> = this.collect(Collectors.toCollection { ArrayList() })

/**Returns this list if mutable, or else copies this to a mutable list.*/
fun <T> List<T>.mutable(): MutableList<T> {
	if(this is MutableList<T>)
		return this
	return ArrayList(this)
}

fun <T> arrayListOfNull(size: Int) = ArrayList<T?>(size).also {
	for(i in 0 until size)
		it.add(null)
}

/**Returns the next item, or null if we're out of items.*/
inline fun <T> Iterator<T>.nextOpt() = if(this.hasNext()) this.next() else null

/**Returns the 1 item that matches the condition. Returns null if zero or multiple items match.*/
inline fun <T> Iterable<T>.find1(condition: (T) -> Boolean): T? {
	var out: T? = null
	for(item in this) {
		if(condition(item)) {
			if(out == null)
				out = item
			else
				return null
		}
	}
	return out
}

fun <T> MutableList<T>.addIfMissing(item: T) {
	if(!this.contains(item))
		this.add(item)
}

inline fun <T> MutableList<T>.update(index: Int, transform: (T) -> T) {
	this[index] = transform(this[index])
}

inline fun <T> MutableList<T>.updateOpt(index: Int, transform: (T) -> T) {
	if(indices.contains(index))
		update(index, transform)
}

/**Clears the list and adds all provided items.*/
fun <T> MutableList<T>.setAll(items: Collection<T>) {
	this.clear()
	this.addAll(items)
}

inline fun <T,R> Collection<T>.mapToArrayList(transform: (T) -> R): ArrayList<R> {
	val out = ArrayList<R>(this.size)
	for(item in this)
		out.add(transform(item))
	return out
}

inline fun <T,R> Collection<T>.mapToHashSet(transform: (T) -> R): HashSet<R> {
	val out = HashSet<R>(this.size)
	for(item in this)
		out.add(transform(item))
	return out
}

inline fun <K,V> MutableMap<K,V>.inlineReplaceAll(transform: (K,V) -> V) {
	val it = this.iterator()
	while(it.hasNext()) {
		val entry = it.next()
		entry.setValue(transform(entry.key, entry.value))
	}
}

/**Transforms each item in the list. If a transform throws an exception, that item is omitted and the mapping continues.*/
inline fun <T,R> List<T>.tryMap(transform: (T) -> R, onFail: (T, Exception) -> Unit = {_, _ ->}): ArrayList<R> {
	val out = ArrayList<R>(size)
	for(item in this) {
		val transformed: R
		try {
			transformed = transform(item)
		} catch(e: Exception) {
			onFail(item, e)
			continue
		}
		out.add(transformed)
	}
	return out
}


inline fun <K,V> Collection<V>.associateByMut(key: (V) -> K) = HashMap<K,V>(this.size).also { map ->
	for(item in this)
		map.putNew(key(item), item)
}
inline fun <K,V> Iterable<V>.associateByMut(key: (V) -> K) = HashMap<K,V>().also { map ->
	for(item in this)
		map.putNew(key(item), item)
}

/**Calls add or set as appropriate, depending on the index.*/
fun <T> MutableList<T>.addOrSet(index: Int, item: T) {
	if(index == this.size)
		this.add(item)
	else
		this.set(index, item)
}

/**Returns the item at this index. If the index is out-of-bounds, returns null instead of throwing.*/
fun <T> List<T>.opt(index: Int): T? = if(index >= 0 && index < size) this[index] else null
fun <T> List<T>.firstOpt(): T? = if(this.isEmpty()) null else this[0]
fun <T> Array<T>.firstOpt(): T? = if(this.isEmpty()) null else this[0]


/**Adds 1 item to a list. If you use the Kotlin plus() function, there will be unexpected bugs when you have `List<List>` (including `List<Path>`).*/
inline fun <T> List<T>.plus1(item: T): ArrayList<T> {
	val out = ArrayList<T>(size + 1)
	out.addAll(this)
	out.add(item)
	return out
}

/**Creates a new array with an item added at the specified position.*/
inline fun <reified T> Array<T>.plusAt(index: Int, item: T): Array<T> {
	val out = Array<T>(size + 1) { item }
	if(index > 0)
		System.arraycopy(this, 0, out, 0, index)
	if(index < this.size)
		System.arraycopy(this, index, out, index + 1, size - index)
	return out
}
/**Creates a new array with an item removed at the specified position.*/
inline fun <reified T> Array<T>.minusAt(index: Int): Array<T> {
	val out = Array<T>(size - 1) { this[0] }
	if(index > 1)
		System.arraycopy(this, 1, out, 1, index - 1)
	if(index < this.size - 1)
		System.arraycopy(this, index + 1, out, index, size - index - 1)
	return out
}

/**Returns a list with an item inserted at the specified index.*/
fun <T> List<T>.plusAt(index: Int, item: T) = ArrayList<T>(size + 1).also { out ->
	for(i in 0 until index)
		out.add(this[i])
	out.add(item)
	for(i in index + 1 until size)
		out.add(this[i])
}

/**Returns a list without the item at the provided index.*/
fun <T> List<T>.minusAt(index: Int): ArrayList<T> = ArrayList<T>(size - 1).also { out ->
	if(index < 0 || index >= this.size)
		throw IllegalArgumentException("Index $index not valid in list of size ${this.size}")
	for(i in 0 until index)
		out.add(this[i])
	for(i in index + 1 until size)
		out.add(this[i])
}

fun <T> MutableList<T>.move(fromIdx: Int, toIdx: Int) {
	val item = removeAt(fromIdx)
	add(toIdx, item)
}

inline fun <T> List<T>.minusFirst() = this.minusAt(0)
inline fun <T> List<T>.minusLast() = this.minusAt(lastIndex)

fun <T> List<T>.withReplacement(index: Int, item: T): ArrayList<T> {
	val out = ArrayList(this)
	out[index] = item
	return out
}
inline fun <T> List<T>.withReplacement(index: Int, transform: (T) -> T): ArrayList<T> {
	val out = ArrayList(this)
	out[index] = transform(out[index])
	return out
}
fun <T> List<T>.withReplacement(old: T, new: T): List<T> {
	val i = indexOf(old)
	if(i == -1)
		throw IllegalStateException("Element not found in list of $size items: $old")
	return this.withReplacement(i, new)
}

inline fun <T> List<T>.plusOrReplace(item: T, finder: (T) -> Boolean): List<T> {
	val i = indexOfFirst(finder)
	return if(i == -1) this.plus(item) else this.withReplacement(i, item)
}

/**Throws an exception if there is not exactly 1 element in this collection.*/
fun <T> Collection<T>.only(): T {
	if(size != 1)
		throw IllegalStateException("Collection contains $size items")
	return first()
}
/**Throws an exception if there is not exactly 1 element in this collection.*/
fun <T> Array<T>.only(): T {
	if(size != 1)
		throw IllegalStateException("Array contains $size items")
	return this[0]
}

inline fun <T> MutableCollection<T>.impRmFirst(predicate: (T) -> Boolean): T? {
	val it = iterator()
	while(it.hasNext()) {
		val t = it.next()
		if(predicate(t)) {
			it.remove()
			return t
		}
	}
	return null
}

fun <T> List<T>.minusAll(selector: (T) -> Boolean): List<T> {
	val out = ArrayList(this)
	out.removeAll(selector)
	return out
}

/**If the map doesn't contain this key, throws an exception that mentions the key in the message.*/
fun <K,V> Map<K,V>.require(key: K): V = this[key] ?: throw Exception("Key not found: $key")

// TODO move
/**Throws an error if the map already contains this key. The value must not be null.
 * @param errorMessage constructs a message using the value that already exists in the map*/
inline fun <K,V> MutableMap<K,V>.putNew(key: K, value: V, crossinline errorMessage: (V) -> String = { "Duplicate key: $key" }) {
	if(value == null)
		throw IllegalArgumentException("value cannot be null")
	this.compute(key) {_,prev ->
		if(prev != null)
			throw IllegalStateException(errorMessage(prev))
		value
	}
}
