package imp.util

import java.util.*

inline fun String.toUuid(): UUID = UUID.fromString(this)
inline fun String.hashedUuid(): UUID = UUID.nameUUIDFromBytes(this.toByteArray())