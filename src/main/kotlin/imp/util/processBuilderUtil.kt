package imp.util

/**Basically a try-catch-finally function: starts the process, calls the block, then forciblyl destroys the process no matter whether an error was thrown.
 * The block will not be called if the process fails to start.*/
fun ProcessBuilder.impRun(block: (Process) -> Unit) {
	val process = this.start()
	try {
		block(process)
	} finally {
		process.destroyForcibly()
	}
}