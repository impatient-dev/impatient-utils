package imp.util

import kotlin.math.max
import kotlin.math.min


const val emDash = '—'


/**Returns a substring if the string is too long.*/
fun String.abbreviate(maxLength: Int): String = if(length <= maxLength) this else this.substring(maxLength)
/**Returns a substring if the string is too long, with an ellipsis on the end. This variant uses a 1-character ellipsis.*/
fun String.abbreviateEllipsis(maxLength: Int): String = if(length <= maxLength) this else this.substring(0, maxLength - 1) + '…'
/**Returns a substring if the string is too long, with an ellipsis on the end. This variant uses a 3-character ellipsis.*/
fun String.abbreviateEllipsis3(maxLength: Int): String = if(length <= maxLength) this else this.substring(0, maxLength - 3) + "..."

fun String.blankToNull(): String? = if(this.isBlank()) null else this
fun String.emptyToNull(): String? = if(this.isEmpty()) null else this

/**Returns "null" or "non-null".*/
fun Any?.isNullStr() = if(this == null) "null" else "non-null"


inline fun String.indexOfFirst(startIdx: Int, predicate: (Char) -> Boolean): Int {
	val out = substring(startIdx).indexOfFirst(predicate)
	return if(out == -1) -1 else out + startIdx
}


/**Returns the substring, clamping any out-of-bounds indexes instead of throwing an exception.
 * Like substring, start is inclusive and end is exclusive.*/
fun String.clampSubstr(start: Int, end: Int): String {
	if(start > length || end <= 0 || end <= start)
		return ""
	return substring(max(0, start), min(length, end))
}
/**Returns the substring, clamping any out-of-bounds index instead of throwing an exception.
 * Like substring, start is inclusive.*/
fun String.clampSubstr(start: Int): String {
	if(start > length)
		return ""
	return substring(max(0, start))
}

/**Like substring, but if the end is -1, it is ignored.
 * Useful when the end comes from indexOf or similar.*/
fun String.substrUntil(start: Int, end: Int): String {
	val b = if(end == -1) length else end
	return substring(start, b)
}
/**Like substring, but if the start is -1, this returns an empty string.
 * Useful when the start comes from indexOf or similar.*/
fun String.substrAfter(start: Int): String {
	return if(start == -1) "" else substring(start)
}

/**Like regular substring, but treats negative numbers as a number of characters before the end.
 * -1 is the last character. TODO unit test*/
fun String.niceSubstr(begin: Int = 0, end: Int = length): String {
	val a = if(begin < 0) length + begin else begin
	val b = if(end < 0) length + end else end
	return substring(a, b)
}


fun String.splitByWhitespace(): ArrayList<String> {
	val out = ArrayList<String>()
	var start = 0 // inclusive
	while(start <= lastIndex) {
		if(this[start].isWhitespace()) {
			start++
			continue
		}
		var end = start + 1 // exclusive
		while(end <= lastIndex && !this[end].isWhitespace())
			end++
		out.add(this.substring(start, end))
		start = end + 1
	}
	return out
}


/**Returns a stacktrace without any code line numbers - just exception classes and messages.
 * Intended to be more user-readable.*/
fun stacktraceNoCode(e: Throwable): String {
	val stacktrace = StringBuilder()
	var ce: Throwable = e
	while(true) {
		stacktrace.append(ce.javaClass.canonicalName)
		stacktrace.append(": ")
		stacktrace.append(ce.message)
		if(ce.cause == null || ce.cause == ce)
			break
		stacktrace.appendLine()
		ce = ce.cause!!
	}
	return stacktrace.toString()
}


fun String.blankAndNotEmpty(): Boolean = isNotEmpty() && isBlank()

/**Capitalizes the first character and leaves the rest of the string alone.*/
fun String.initialCap(): String {
	if(isEmpty()) return this
	val c = this[0]
	if(c.isLowerCase())
		return this.replaceFirstChar { c.uppercaseChar() }
	return this
}


/**Makes the first character uppercase, and the other characters lowercase.*/
fun String.onlyInitialCap(): String = when(length) {
	0 -> this
	1 -> this.uppercase()
	else -> this[0].uppercaseChar() + this.substring(1).lowercase()
}