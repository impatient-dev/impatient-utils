package imp.util


/**Returns this, unless the block throws, in which case this object is closed and the exception is rethrown.
 * Intended for when you've opened a resource and need to initialize something before returning the resource to whoever asked for it.
 * If you wrap your initialization code in this function, a failure to initialize won't leak the resource.*/
inline fun <T: AutoCloseable> T.closeOnFail(block: (T) -> Unit): T {
	try {
		block(this)
		return this
	} catch(e: Throwable) {
		this.close()
		throw e
	}
}

/**Runs the block, and returns whatever the block returns.
 * If the block throws, the AutoCloseable will be closed and the exception rethrown.
 * Intended for when you've opened a resource and need to initialize and return something to whoever asked for it.
 * If you wrap your initialization code in this function, a failure to initialize won't leak the AutoCloseable.*/
inline fun <T: AutoCloseable, R> T.closeOnFailR(block: (T) -> R): R {
	try {
		return block(this)
	} catch(e: Throwable) {
		this.close()
		throw e
	}
}


/**Lazily creates an item that is AutoCloseable.*/
class LazyAutoCloseable <T: AutoCloseable> (
	private val factory: () -> T,
) : AutoCloseable {
	private var initialized = false
	private var _it: T? = null

	val it: T get() {
		if(!initialized) {
			_it = factory()
			initialized = true
		}
		return _it!!
	}

	override fun close() {
		_it?.close()
	}
}