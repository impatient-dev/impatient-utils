package imp.util


/**If the block throws an Eception, returns null instead.*/
inline fun <T> exToNull(block: () -> T?): T? {
	try {
		return block()
	} catch(e: Exception) {
		return null
	}
}


/**If the block throws an Exception, wraps it with one having the provided message, to provide more context on what went wrong.*/
inline fun <T> exCtx(msg: () -> String, block: () -> T): T {
	try {
		return block()
	} catch(e: Exception) {
		throw Exception(msg(), e)
	}
}