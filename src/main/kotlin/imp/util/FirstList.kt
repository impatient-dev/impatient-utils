package imp.util


/**Stores the first few items added, then ignores further attempts to add new items.
 * For a list that remembers the extra items, see PossiblyTruncatedList.*/
class FirstList <T> (
	/**The number of items this list can store.*/
	val capacity: Int,
) : List<T> {
	private val list = ArrayList<T>()

	override val size get() = list.size
	override fun isEmpty() = list.isEmpty()
	override operator fun get(index: Int): T = list[index]
	override fun iterator() = list.iterator()
	override fun contains(element: T) = list.contains(element)
	override fun containsAll(elements: Collection<T>) = list.containsAll(elements)

	/**Creates a list with the specified items and no extra capacity.*/
	constructor(src: Collection<T>) : this(src.size) {
		list.addAll(src)
		list.trimToSize()
	}

	/**Returns false if this list as at capacity and nothing was added.*/
	fun add(item: T): Boolean {
		if(size >= capacity)
			return false
		list.add(item)
		if(size == capacity)
			list.trimToSize()
		return true
	}

	override fun hashCode() = list.hashCode()
	override fun equals(other: Any?) = if(other is FirstList<*>) this.list == other.list else this.list == other

	override fun indexOf(element: T) = list.indexOf(element)
	override fun lastIndexOf(element: T) = list.lastIndexOf(element)
	override fun listIterator() = list.listIterator()
	override fun listIterator(index: Int) = list.listIterator(index)
	override fun subList(fromIndex: Int, toIndex: Int) = list.subList(fromIndex, toIndex)
}