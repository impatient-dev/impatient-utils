package imp.util

import java.io.InputStream
import java.io.OutputStream
import java.io.PrintWriter
import java.io.Writer
import java.nio.charset.Charset
import java.nio.file.Files
import java.nio.file.Path
import kotlin.io.path.createDirectories
import kotlin.io.path.writeText


/**Calls resolve, but with some checks to ensure the path is relative and does not try to "escape" (../foo).*/
fun Path.checkedResolve(path: Path): Path {
	if(path.isAbsolute)
		throw IllegalArgumentException("Path must be relative: $path")
	val out = this.resolve(path)
	if(!out.startsWith(this) && out != path) // out==path happens if this is ""
		throw IllegalArgumentException("Suspicious path: $path")
	return out
}

/**Reads all available bytes into a string and returns it. Does not block.*/
fun InputStream.availableToString(charset: Charset = Charsets.UTF_8): String {
	val out = StringBuilder()
	val buffer = ByteArray(DEFAULT_BUFFER_SIZE)
	while(true) {
		val a = this.available()
		if(a == 0)
			break
		val i = this.read(buffer, 0, a)
		if(i == -1)
			break
		out.append(String(buffer, 0, i, charset))
	}
	return out.toString()
}


/**Returns a buffered output stream into this file. If the parent directory doesn't exist, that is created first.*/
fun Path.impFileOut(): OutputStream {
	this.parent?.createDirectories()
	return this.toFile().outputStream().buffered()
}

/**Returns a buffered writer into this file. If the parent directory doesn't exist, that is created first.*/
fun Path.impFileWriter(): Writer {
	Files.createDirectories(this.parent!!)
	return this.toFile().writer().buffered()
}

/**Writes text to a file, replacing any existing content. Creates the directory if necessary.*/
fun Path.impWrite(str: String) {
	Files.createDirectories(this.parent!!)
	this.writeText(str)
}


/**Lists paths in the directory (not recursive). If the directory doesn't exist, nothing happens.*/
inline fun Path.impDirList(block: (Path) -> Unit) {
	if(!Files.exists(this))
		return
	Files.newDirectoryStream(this).use { stream ->
		stream.forEach(block)
	}
}


inline fun OutputStream.printWriter() = PrintWriter(this)
inline fun Writer.printWriter() = PrintWriter(this)