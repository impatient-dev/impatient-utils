package imp.util


/**Returns a sequence containing only the elements that are of the type [R].*/
inline fun <reified R> Sequence<*>.filterToType(): Sequence<R> = this
	.map { it as? R }
	.filter { it != null }
	.map { it!! }