package imp.util

import kotlin.math.*


/**Computes the sine of an amount in degrees.*/
fun sinDeg(f: Double): Double = sin(f.radians())
/**Computes the cosine of an amount in degrees.*/
fun cosDeg(f: Double): Double = cos(f.radians())
/**Computes the tangent of an amount in degrees.*/
fun tanDeg(f: Double): Double = tan(f.radians())

/**Computes the arc sine of an amount in [-1,1], and returns the result in degrees.*/
fun asinDeg(f: Double): Double = asin(f).degrees()
/**Computes the arc cosine of an amount in [-1,1], and returns the result in degrees.*/
fun acosDeg(f: Double): Double = acos(f).degrees()
/**Computes the arc tangent of an amount in [-1,1], and returns the result in degrees.*/
fun atanDeg(f: Double): Double = atan(f).degrees()