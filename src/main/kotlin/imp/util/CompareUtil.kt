package imp.util

object CompareUtil {
	/**A comparator function that considers null less than any other value.*/
	fun <T: Comparable<T>> nullsLess(a: T?, b: T?): Int {
		if(a == null)
			return if(b == null) 0 else -1
		if(b == null)
			return 1
		return a.compareTo(b)
	}

	/**A comparator function that considers null greater than any other value.*/
	fun <T: Comparable<T>> nullsGreater(a: T?, b: T?): Int {
		if(a == null)
			return if(b == null) 0 else 1
		if(b == null)
			return -1
		return a.compareTo(b)
	}
}


/**Returns the value, unless it's outside [min, max].*/
fun <T: Comparable<T>> clamp(min: T, value: T, max: T): T {
	if(value < min)
		return min
	if(value > max)
		return max
	return value
}


/**Returns the smallest item. Returns null if there are no non-null items.*/
fun <T : Comparable<T>> Iterable<T?>.impMin(): T? {
	var out: T? = null
	for(item in this)
		if(out == null || (item != null && item < out))
			out = item
	return out
}
/**Returns the smallest item. Returns null if there are no non-null items.*/
fun <T : Comparable<T>> Array<T?>.impMin(): T? {
	var out: T? = null
	for(item in this)
		if(out == null || (item != null && item < out))
			out = item
	return out
}


/**Returns the largest item. Returns null if there are no non-null items.*/
fun <T : Comparable<T>> Iterable<T?>.impMax(): T? {
	var out: T? = null
	for(item in this)
		if(out == null || (item != null && item > out))
			out = item
	return out
}
/**Returns the largest item. Returns null if there are no non-null items.*/
fun <T : Comparable<T>> Array<T?>.impMax(): T? {
	var out: T? = null
	for(item in this)
		if(out == null || (item != null && item > out))
			out = item
	return out
}



/**Returns the smallest item. Returns null if there are no non-null items. The provided comparator doesn't have to handle null.*/
fun <T> Iterable<T?>.impMin(comparator: Comparator<T>) : T? {
	var out: T? = null
	for(item in this)
		if(out == null || (item != null && comparator.compare(item, out) < 0))
			out = item
	return out
}
/**Returns the smallest item. Returns null if there are no non-null items. The provided comparator doesn't have to handle null.*/
fun <T> Array<T?>.impMin(comparator: Comparator<T>) : T? {
	var out: T? = null
	for(item in this)
		if(out == null || (item != null && comparator.compare(item, out) < 0))
			out = item
	return out
}


/**Returns the largest item. Returns null if there are no non-null items. The provided comparator doesn't have to handle null.*/
fun <T> Iterable<T?>.impMax(comparator: Comparator<T>) : T? {
	var out: T? = null
	for(item in this)
		if(out == null || (item != null && comparator.compare(item, out) > 0))
			out = item
	return out
}
/**Returns the largest item. Returns null if there are no non-null items. The provided comparator doesn't have to handle null.*/
fun <T> Array<T?>.impMax(comparator: Comparator<T>) : T? {
	var out: T? = null
	for(item in this)
		if(out == null || (item != null && comparator.compare(item, out) > 0))
			out = item
	return out
}


/**Returns the smallest item, applying the transform before comparing items. Returns null if there are no non-null items. The provided transform doesn't have to handle null.*/
fun <T, C : Comparable<C>> Iterable<T?>.impMinBy(transform: (T) -> C) : T? {
	var out: T? = null
	var outTransformed: C? = null
	for(item in this) {
		if(item == null)
			continue
		val transformed = transform(item)
		if(outTransformed == null || outTransformed < transformed) {
			out = item
			outTransformed = transformed
		}
	}
	return out
}
/**Returns the smallest item, applying the transform before comparing items. Returns null if there are no non-null items. The provided transform doesn't have to handle null.*/
fun <T, C : Comparable<C>> Array<T?>.impMinBy(transform: (T) -> C) : T? {
	var out: T? = null
	var outTransformed: C? = null
	for(item in this) {
		if(item == null)
			continue
		val transformed = transform(item)
		if(outTransformed == null || outTransformed > transformed) {
			out = item
			outTransformed = transformed
		}
	}
	return out
}


/**Returns the largest item, applying the transform before comparing items. Returns null if there are no non-null items. The provided transform doesn't have to handle null.*/
fun <T, C : Comparable<C>> Iterable<T?>.impMaxBy(transform: (T) -> C) : T? {
	var out: T? = null
	var outTransformed: C? = null
	for(item in this) {
		if(item == null)
			continue
		val transformed = transform(item)
		if(outTransformed == null || outTransformed > transformed) {
			out = item
			outTransformed = transformed
		}
	}
	return out
}
/**Returns the largest item, applying the transform before comparing items. Returns null if there are no non-null items. The provided transform doesn't have to handle null.*/
fun <T, C : Comparable<C>> Array<T?>.impMaxBy(transform: (T) -> C) : T? {
	var out: T? = null
	var outTransformed: C? = null
	for(item in this) {
		if(item == null)
			continue
		val transformed = transform(item)
		if(outTransformed == null || outTransformed < transformed) {
			out = item
			outTransformed = transformed
		}
	}
	return out
}



fun  <T: Comparable<T>> lesser(a: T, b: T) = if(a < b) a else b