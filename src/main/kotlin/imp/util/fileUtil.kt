package imp.util

import org.apache.commons.io.FileUtils
import java.nio.file.Files
import java.nio.file.Path

/**Deletes any files or subdirectories in the directory (recursively), if the directory exists.*/
fun Path.impClean() {
	if(Files.exists(this))
		FileUtils.cleanDirectory(this.toFile())
}

/**Deletes the file or directory, if it exists, without suppressing exceptions.*/
fun Path.impDelete() {
	if(Files.exists(this))
		FileUtils.forceDelete(this.toFile())
}