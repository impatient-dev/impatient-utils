package imp.util.fmt

inline fun Byte.hexStr() = this.toString(16)
inline fun Short.hexStr() = this.toString(16)
inline fun Int.hexStr() = this.toString(16)
inline fun Long.hexStr() = this.toString(16)

/**Unsigned.*/
inline fun Byte.uHexStr() = this.toUByte().toString(16)
/**Unsigned.*/
inline fun Short.uHexStr() = this.toUShort().toString(16)
/**Unsigned.*/
inline fun Int.uHexStr() = this.toUInt().toString(16)
/**Unsigned.*/
inline fun Long.uHexStr() = this.toULong().toString(16)