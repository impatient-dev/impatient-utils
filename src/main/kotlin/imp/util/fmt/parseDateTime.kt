package imp.util.fmt

import imp.util.clampSubstr
import imp.util.indexOfFirst
import imp.util.substrAfter
import imp.util.substrUntil
import java.time.*
import java.util.regex.Pattern


/**Various pieces of time-related info that can be specified by the user. Each piece can be specified only once; an exception is thrown if duplicate inputs are provided.*/
private data class UserDateTimeInput (
	val year: Int? = null,
	val month: Month? = null,
	/**1-31*/
	val dayOfMonth: Int? = null,
	val hour: Int? = null,
	val minute: Int? = null,
	val second: Int? = null,
) {
	fun plusDate(date: LocalDate): UserDateTimeInput {
		check(year == null && month == null && dayOfMonth == null)
		return copy(year = date.year, month = date.month, dayOfMonth = date.dayOfMonth)
	}
	fun plusTime(time: LocalTime): UserDateTimeInput {
		check(hour == null && minute == null && second == null)
		return copy(hour = time.hour, minute = time.minute, second = time.second)
	}

	fun plusYear(y: Int): UserDateTimeInput {
		check(year == null)
		return copy(year = y)
	}
	fun plusMonth(m: Month): UserDateTimeInput {
		check(month == null)
		return copy(month = m)
	}
	fun plusDayOfMonth(d: Int): UserDateTimeInput {
		check(dayOfMonth == null)
		return copy(dayOfMonth = d)
	}

	fun toLocalDateTime(now: LocalDateTime): LocalDateTime {
		return LocalDateTime.of(year ?: now.year, month ?: now.month, dayOfMonth ?: now.dayOfMonth, hour ?: now.hour, minute ?: now.minute, second ?: now.second)
	}
}




/**Returns true for characters such as spaces and commas, which can separate different parts of a date/time and do not get any parsing.*/
private fun ignorable(c: Char): Boolean = when(c) {
	' ', ',', ';', '.' -> true
	else -> false
}
/**Returns true if the character is ignorable, or if there's no such character because the string isn't that long.*/
private fun notNotIgnorable(s: String, i: Int): Boolean = if(i < s.length) ignorable(s[i]) else true




/**Parses user input into a LocalDateTime.
 * The user can enter as much or as little as he wants to; the missing inputs will default to now.
 * @param future if true, the user is probably trying to enter a date in the future and we should default to that.
 * If false, the user is probably trying to enter a date in the past; if null, there is no expectation and we can't default either way.*/
fun parseDateTime(str: String, now: LocalDateTime = LocalDateTime.now(), future: Boolean? = null): LocalDateTime {
	var s = str.trimStart(::ignorable)
	if(s.isEmpty())
		return now
	var parsed = UserDateTimeInput()
	while(!s.isEmpty()) {
		val next = AllDateTimePartParsers.parseFromStart(s, parsed, now, future)
		check(next != null) { "Unable to parse the remaining input: $s" }
		parsed = next.first
		s = s.clampSubstr(next.second).trimStart(::ignorable)
	}
	return parsed.toLocalDateTime(now)
}




private interface DateTimePartParser {
	/**Tries to parse some info from the start of a user-supplied date/time.
	 * Returns the information gleaned from the input, plus the first index after the part of the input that was looked at.
	 * Returns null if this parser can't extract anything useful from the current start of the string.
	 * The input must be trimmed of any ignorable leading characters.
	 * @param future if true, the user is probably trying to enter a date in the future and we should default to that.
	 * If false, the user is probably trying to enter a date in the past; if null, there is no expectation and we can't default either way.*/
	fun parseFromStart(s: String, i: UserDateTimeInput, now: LocalDateTime, future: Boolean?): Pair<UserDateTimeInput, Int>?
}


/**Tries all other parsers, and returns the first non-null result (if any).*/
private object AllDateTimePartParsers : DateTimePartParser {
	private val parsers = listOf<DateTimePartParser>(
		DatePartParser,
		YearPartParser,
		MonthPartParser,
		DayPartParser,
		DayOfWeekPartParser,
		TimePartParser,
	)

	override fun parseFromStart(s: String, i: UserDateTimeInput, now: LocalDateTime, future: Boolean?): Pair<UserDateTimeInput, Int>? {
		for(p in parsers) {
			val out = p.parseFromStart(s, i, now, future)
			if(out != null) {
				assert(out.second >= 0) { "Got invalid next index ${out.second} from $p while parsing $s" }
				return out
			}
		}
		return null
	}
}


private object DatePartParser : DateTimePartParser {
	private val REGEX_YMD = Pattern.compile("[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}")
	private val REGEX_MD = Pattern.compile("[0-9]{1,2}-[0-9]{1,2}")

	override fun parseFromStart(s: String, i: UserDateTimeInput, now: LocalDateTime, future: Boolean?): Pair<UserDateTimeInput, Int>? {
		var m = REGEX_YMD.matcher(s)
		if(m.lookingAt() && notNotIgnorable(s, m.end())) {
			val sep1 = s.indexOf('-')
			val sep2 = s.indexOf('-', sep1 + 1)
			val year = s.substring(0, sep1).toInt()
			val month = Month.of(s.substring(sep1 + 1, sep2).toInt())
			val day = s.substring(sep2 + 1, m.end()).toInt()
			return Pair(i.plusDate(LocalDate.of(year, month, day)), m.end() + 1)
		}

		m = REGEX_MD.matcher(s)
		if(m.lookingAt() && notNotIgnorable(s, m.end())) {
			val sep = s.indexOf('-')
			val date = now.toLocalDate()
				.withMonth(s.substring(0, sep).toInt())
				.withDayOfMonth(s.substring(sep + 1, m.end()).toInt())
			return Pair(i.plusDate(date), m.end() + 1)
		}

		return null
	}
}


private object YearPartParser : DateTimePartParser {
	private val REGEX_Y = Pattern.compile("[0-9]{4}")

	override fun parseFromStart(s: String, i: UserDateTimeInput, now: LocalDateTime, future: Boolean?): Pair<UserDateTimeInput, Int>? {
		val m = REGEX_Y.matcher(s)
		if(m.lookingAt() && notNotIgnorable(s, m.end())) {
			val y = m.group().toInt()
			if(y >= 1900 && y <= 3000)
				return Pair(i.plusYear(y), m.end() + 1)
		}
		return null
	}
}


private object MonthPartParser : DateTimePartParser {
	private val months = listOf(
		Pair("january", Month.JANUARY),
		Pair("jan", Month.JANUARY),
		Pair("february", Month.FEBRUARY),
		Pair("feb", Month.FEBRUARY),
		Pair("march", Month.MARCH),
		Pair("mar", Month.MARCH),
		Pair("april", Month.APRIL),
		Pair("apr", Month.APRIL),
		Pair("may", Month.MAY),
		Pair("june", Month.JUNE),
		Pair("jun", Month.JUNE),
		Pair("july", Month.JULY),
		Pair("jul", Month.JULY),
		Pair("august", Month.AUGUST),
		Pair("aug", Month.AUGUST),
		Pair("september", Month.SEPTEMBER),
		Pair("sep", Month.SEPTEMBER),
		Pair("october", Month.OCTOBER),
		Pair("oct", Month.OCTOBER),
		Pair("november", Month.NOVEMBER),
		Pair("nov", Month.NOVEMBER),
		Pair("december", Month.DECEMBER),
		Pair("dec", Month.DECEMBER),
	)

	override fun parseFromStart(s: String, i: UserDateTimeInput, now: LocalDateTime, future: Boolean?): Pair<UserDateTimeInput, Int>? {
		for(m in months) {
			if(s.startsWith(m.first) && notNotIgnorable(s, m.first.length))
				return Pair(i.plusMonth(m.second), m.first.length + 1)
		}
		return null
	}
}


private object DayPartParser : DateTimePartParser {
	private val TOMORROW = "tomorrow"
	private val YESTERDAY = "yesterday"

	override fun parseFromStart(s: String, i: UserDateTimeInput, now: LocalDateTime, future: Boolean?): Pair<UserDateTimeInput, Int>? {
		if(s.startsWith(TOMORROW, true) && notNotIgnorable(s, TOMORROW.length))
			return Pair(i.plusDate(now.toLocalDate().plusDays(1)), TOMORROW.length + 1)
		if(s.startsWith(YESTERDAY, true) && notNotIgnorable(s, YESTERDAY.length))
			return Pair(i.plusDate(now.toLocalDate().minusDays(1)), YESTERDAY.length + 1)
		return null
	}
}


private object DayOfWeekPartParser : DateTimePartParser {
	private val days = listOf(
		Pair("monday", DayOfWeek.MONDAY),
		Pair("mon", DayOfWeek.MONDAY),
		Pair("mo", DayOfWeek.MONDAY),
		Pair("m", DayOfWeek.MONDAY),
		Pair("tuesday", DayOfWeek.TUESDAY),
		Pair("tues", DayOfWeek.TUESDAY),
		Pair("tue", DayOfWeek.TUESDAY),
		Pair("tu", DayOfWeek.TUESDAY),
		Pair("t", DayOfWeek.TUESDAY),
		Pair("wednesday", DayOfWeek.WEDNESDAY),
		Pair("wed", DayOfWeek.WEDNESDAY),
		Pair("we", DayOfWeek.WEDNESDAY),
		Pair("w", DayOfWeek.WEDNESDAY),
		Pair("thursday", DayOfWeek.THURSDAY),
		Pair("thurs", DayOfWeek.THURSDAY),
		Pair("thu", DayOfWeek.THURSDAY),
		Pair("th", DayOfWeek.THURSDAY),
		Pair("r", DayOfWeek.THURSDAY),
		Pair("friday", DayOfWeek.FRIDAY),
		Pair("fri", DayOfWeek.FRIDAY),
		Pair("fr", DayOfWeek.FRIDAY),
		Pair("f", DayOfWeek.FRIDAY),
		Pair("saturday", DayOfWeek.SATURDAY),
		Pair("sat", DayOfWeek.SATURDAY),
		Pair("sa", DayOfWeek.SATURDAY),
		Pair("a", DayOfWeek.SATURDAY),
		Pair("sunday", DayOfWeek.SUNDAY),
		Pair("sun", DayOfWeek.SUNDAY),
		Pair("su", DayOfWeek.SUNDAY),
		Pair("s", DayOfWeek.SUNDAY),
	)
	private val unambiguousDayDiffs = arrayOf(-3L, -2, -1, 1, 2, 3)

	override fun parseFromStart(s: String, i: UserDateTimeInput, now: LocalDateTime, future: Boolean?): Pair<UserDateTimeInput, Int>? {
		for(d in days) {
			if(s.startsWith(d.first, true) && notNotIgnorable(s, d.first.length)) {
				if(future == null)
					return parseUnspecifiedPastFuture(d.second, i, now)?.let { Pair(it, d.first.length + 1) }
				val direction = if(future) 1L else -1L
				var date = now.toLocalDate().plusDays(direction)
				while(date.dayOfWeek != d.second)
					date = date.plusDays(direction)
				return Pair(i.plusDate(date), d.first.length + 1)
			}
		}
		return null
	}

	private fun parseUnspecifiedPastFuture(day: DayOfWeek, i: UserDateTimeInput, now: LocalDateTime): UserDateTimeInput? {
		for(days in unambiguousDayDiffs) {
			if(day == now.dayOfWeek + days)
				return i.plusDate(now.toLocalDate().plusDays(days))
		}
		return null
	}
}


private object TimePartParser : DateTimePartParser {
	private const val PATTERN_AMPM = " *(am|pm|a\\.m\\.|p\\.m\\.|a|p)"
	private val REGEX_HM = Pattern.compile("[0-9]{1,2}:[0-9]{1,2}$PATTERN_AMPM?", Pattern.CASE_INSENSITIVE)
	private val REGEX_H = Pattern.compile("[0-9]{1,2}$PATTERN_AMPM", Pattern.CASE_INSENSITIVE)

	override fun parseFromStart(s: String, i: UserDateTimeInput, now: LocalDateTime, future: Boolean?): Pair<UserDateTimeInput, Int>? {
		var m = REGEX_HM.matcher(s)
		if(m.lookingAt() && notNotIgnorable(s, m.end())) {
			val colon = s.indexOf(':')
			val h = s.substring(0, colon).toInt()
			val end = s.indexOfFirst(colon + 1) { !it.isDigit() }
			val min = s.substrUntil(colon + 1, end).toInt()
			val pm = pm(s.substrAfter(end))
			return Pair(i.plusTime(time(h, min, pm)), m.end())
		}

		m = REGEX_H.matcher(s)
		if(m.lookingAt() && notNotIgnorable(s, m.end())) {
			val end = s.indexOfFirst { !it.isDigit() }
			val h = s.substring(0, end).toInt()
			if(h > 23)
				return null
			val pm = pm(s.substring(end))
			return Pair(i.plusTime(time(h, 0, pm)), m.end())
		}

		return null
	}

	/**Returns false for AM, true for PM, null for neither.*/
	private fun pm(str: String): Boolean? {
		if(str.contains('p', true))
			return true
		if(str.contains('a', true))
			return false
		return null
	}

	private fun mapHour(h: Int, pm: Boolean?): Int {
		check(h < 24 && h >= 0)
		if(pm == null)
			return h
		if(h == 12)
			return if(pm) 12 else 0
		if(pm)
			return h + 12
		return h
	}

	private fun time(h: Int, m: Int, pm: Boolean?) = LocalTime.of(mapHour(h, pm), m)
}