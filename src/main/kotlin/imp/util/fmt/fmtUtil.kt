package imp.util.fmt


object Millis {
	const val SECOND = 1000L
	const val MINUTE = 60 * SECOND
	const val HOUR = 60 * MINUTE
	const val DAY = 24 * HOUR
	const val WEEK = 7 * DAY
	const val YEAR = 365 * DAY
}


/**Returns a string with the requested number of decimal places, and as many numbers before the decimal point as are necessary.*/
inline fun Double.decimalPlaces(places: Int) = String.format("%1.${places}f", this)
/**Converts this fraction (0-1, etc.) to a percent (0-100, etc.) and formats it with the requested number of decimal places.*/
fun Double.fmtToPercent(decimalPlaces: Int = 0) = String.format("%1.${decimalPlaces}f%%", this * 100)

/**123,456,789.*/
inline fun Int.commaStr(): String = String.format("%,d", this)
/**123,456,789.*/
inline fun Long.commaStr(): String = String.format("%,d", this)