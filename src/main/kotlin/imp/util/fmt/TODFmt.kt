package imp.util.fmt

import imp.util.fmt.Millis.HOUR
import imp.util.fmt.Millis.MINUTE
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.ZonedDateTime

/**Formats a time of day, abstracting the difference between 12-hour and 24-hour time.*/
interface TODFmt {
	/**Formats a time of day from a 24-hour time (hour in [0, 23], minute in [0,59]).*/
	fun hm(hour: Int, minute: Int): String

	/**Formats a time of day, input as milliseconds since midnight.*/
	fun millisHM(millis: Long): String = hm((millis / HOUR).toInt(), ((millis % HOUR) / MINUTE).toInt())
	fun hm(time: LocalTime): String = hm(time.hour, time.minute)
	fun hm(time: LocalDateTime): String = hm(time.hour, time.minute)
	fun hm(time: ZonedDateTime): String = hm(time.hour, time.minute)

	/**Date plus hour and minute.*/
	fun dateHm(time: LocalDateTime): String = "${time.year}-${time.monthValue}-${time.dayOfMonth} ${hm(time)}"
	/**Date plus hour and minute.*/
	fun dateHm(time: ZonedDateTime): String = "${time.year}-${time.monthValue}-${time.dayOfMonth} ${hm(time)}"
}


/**12-hour time.*/
object TOD12 : TODFmt {
	private val am = " a.m."
	private val pm = " p.m."

	override fun hm(hour: Int, minute: Int): String {
		val h = if(hour == 0) 12
			else if(hour > 12) hour - 12
			else hour
		val m = if(minute < 10) "0$minute" else minute.toString()
		val end = if(hour < 12) am else pm
		return "$h:$m$end"
	}
}


/**24-hour time.*/
object TOD24 : TODFmt {
	override fun hm(hour: Int, minute: Int): String {
		val m = if(minute < 10) "0$minute" else minute.toString()
		return "$hour:$m"
	}
}