package imp.util.fmt

import imp.util.fmt.Millis.DAY
import imp.util.fmt.Millis.HOUR
import imp.util.localDateTimeFromEpochMilli
import java.lang.System.currentTimeMillis
import java.time.DayOfWeek
import java.time.ZoneId
import java.time.format.TextStyle
import java.util.*

object TimeFmt {

	/**Formats a time before the current time, for use in a context where it's clear this time is in the past (e.g. after the word "since" or "started").
	 * The output always includes hour+minute, and will include more if the time is far in the past.*/
	fun pastApprox(ms: Long, fmt: TODFmt, nowMs: Long = currentTimeMillis(), zone: ZoneId = ZoneId.systemDefault()): String {
		val time = localDateTimeFromEpochMilli(ms, zone)
		val now = localDateTimeFromEpochMilli(nowMs, zone)
		val diff = nowMs - ms
		val out = fmt.hm(time)

		if(diff > 0) {
			if (diff < 8 * HOUR) return out
			if (diff < 20 * HOUR && time.dayOfMonth == now.dayOfMonth) return out
			if (diff < 5 * DAY) return "${time.dayOfWeek.getDisplayName(TextStyle.FULL, Locale.getDefault())} $out"
		}
		return "${time.year}-${time.monthValue}-${time.dayOfMonth} $out"
	}
}



/**Returns a user-friendly string like "Monday".*/
fun DayOfWeek.impStr() = when(this) {
	DayOfWeek.MONDAY -> "Monday"
	DayOfWeek.TUESDAY -> "Tuesday"
	DayOfWeek.WEDNESDAY -> "Wednesday"
	DayOfWeek.THURSDAY -> "Thursday"
	DayOfWeek.FRIDAY -> "Friday"
	DayOfWeek.SATURDAY -> "Saturday"
	DayOfWeek.SUNDAY -> "Sunday"
}
/**Returns a short user-friendly string like "Mon".*/
fun DayOfWeek.impShort() = when(this) {
	DayOfWeek.MONDAY -> "Mon"
	DayOfWeek.TUESDAY -> "Tue"
	DayOfWeek.WEDNESDAY -> "Wed"
	DayOfWeek.THURSDAY -> "Thu"
	DayOfWeek.FRIDAY -> "Fri"
	DayOfWeek.SATURDAY -> "Sat"
	DayOfWeek.SUNDAY -> "Sun"
}