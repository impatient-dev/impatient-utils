package imp.util.fmt

import imp.util.fmt.Millis.DAY
import imp.util.fmt.Millis.HOUR
import imp.util.fmt.Millis.MINUTE
import imp.util.fmt.Millis.SECOND
import imp.util.fmt.Millis.YEAR
import kotlin.math.roundToInt

private inline fun oneLetterDuration(ms: Long, unit: Long, unitStr: String): String =
	"${(ms.toFloat() / unit).roundToInt()}$unitStr"

/**If keepZero is false, we'll omit the smallest unit if it's zero.*/
private inline fun twoLetterDuration(a: Long, aUnit: String, b: Long, bUnit: String, keepZero: Boolean = false): String =
	if(b == 0L && !keepZero) "$a$aUnit" else "$a$aUnit $b$bUnit"

private inline fun twoLetterDuration(ms: Long, bigUnit: Long, bigStr: String, smallUnit: Long, smallStr: String, keepZero: Boolean = false): String =
	twoLetterDuration(ms / bigUnit, bigStr, (ms % bigUnit) / smallUnit, smallStr, keepZero)

private inline fun threeLetterDuration(
	ms: Long,
	unit1: Long, str1: String,
	unit2: Long, str2: String,
	unit3: Long, str3: String,
	/**If false, we'll omit smaller units if they're zero.*/
	keepZero: Boolean = false,
): String {
	val val1 = ms / unit1
	val remaining = ms % unit1
	return "$val1$str1 ${if(remaining == 0L && !keepZero) "" else twoLetterDuration(remaining, unit2, str2, unit3, str3)}"
}

/**Formats times using letters like H/M/S, e.g. "2h".
 * The results are intended to be user-friendly, and never include milliseconds.*/
object TimeLetterFmt {

	/**Formats a duration (which must be positive) into something with 2 units, like "2h 0m".*/
	fun duration2(ms: Long): String = when {
		ms < 0 -> throw IllegalArgumentException("Negative duration: $ms")
		ms < SECOND -> "0s"
		ms < MINUTE -> oneLetterDuration(ms, SECOND, "s")
		ms < HOUR -> twoLetterDuration(ms, MINUTE, "m", SECOND, "s", keepZero = true)
		ms < DAY -> twoLetterDuration(ms, HOUR, "h", MINUTE, "m", keepZero = true)
		ms < YEAR -> twoLetterDuration(ms, DAY, "d", HOUR, "h", keepZero = true)
		else -> twoLetterDuration(ms, YEAR, "y", DAY, "d", keepZero = true)
	}
	
	/**Formats a duration (which must be positive) into something fairly short like "2h 5m" or "15h.*/
	fun durationApprox(ms: Long): String = when {
		ms < 0 -> throw IllegalArgumentException("Negative duration: $ms")
		ms < SECOND -> "0s"
		ms < MINUTE -> oneLetterDuration(ms, SECOND, "s")
		ms < 10 * MINUTE -> twoLetterDuration(ms, MINUTE, "m", SECOND, "s")
		ms < HOUR -> oneLetterDuration(ms, MINUTE, "m")
		ms < 10 * HOUR -> twoLetterDuration(ms, HOUR, "h", MINUTE, "m")
		ms < DAY -> oneLetterDuration(ms, HOUR, "h")
		ms < 10 * DAY -> twoLetterDuration(ms, DAY, "d", HOUR, "h")
		ms < YEAR -> oneLetterDuration(ms, DAY, "d")
		ms < 10 * YEAR -> twoLetterDuration(ms, YEAR, "y", DAY, "d")
		else -> oneLetterDuration(ms, YEAR, "y")
	}

	/**Formats a duration (which must be positive) that always includes seconds, and maybe also minutes and greater.*/
	fun durationToS(ms: Long): String = when {
		ms < 0 -> throw IllegalArgumentException("Negative duration: $ms")
		ms < SECOND -> "0s"
		ms < MINUTE -> oneLetterDuration(ms, SECOND, "s")
		ms < HOUR -> twoLetterDuration(ms, MINUTE, "m", SECOND, "s", keepZero = true)
		else -> threeLetterDuration(ms, HOUR, "h", MINUTE, "m", SECOND, "s", keepZero = true)
	}
}