package imp.util

/**Suppresses InterruptedException.*/
inline fun catchInterrupt(block: () -> Unit) {
	try { block() } catch(e: InterruptedException) {}
}

/**Suppresses InterruptedException.*/
fun sleepQuietly(millis: Long) {
	try {
		Thread.sleep(millis)
	} catch(e: InterruptedException) {
		//ignore
	}
}