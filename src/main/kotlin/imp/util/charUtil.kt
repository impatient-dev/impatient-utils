package imp.util

fun Char.repeat(times: Int): String {
	if(times == 0)
		return ""
	else if(times < 0)
		throw IllegalArgumentException("times: $times")
	val out = StringBuilder(times)
	for(i in 0 until times)
		out.append(this)
	return out.toString()
}