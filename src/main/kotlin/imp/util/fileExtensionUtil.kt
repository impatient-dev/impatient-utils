package imp.util


/**Strips the file extension from this string, including the period, if there is one. This function never throws.*/
fun String.withoutFileExtension(): String {
	val i = indexOf('.')
	return if(i == -1) this else this.substring(0, i)
}

/**Returns a pair of (name without extension, extension).*/
fun String.splitFileExtension(): Pair<String, String?> {
	val i = indexOf('.')
		if(i == -1 || i == length - 1)
			return Pair(this, null)
		else
			return Pair(substring(0, i), substring(i + 1, length))
}