package imp.util

import java.util.concurrent.atomic.AtomicReference


/**Atomically sets the value, only if it == the expected value.
 * Returns the value stored in this AtomicReference, which is generally the new one or the one that != the expectation.*/
fun <T> AtomicReference<T>.compareSetGet(expected: T, new: T): T {
	return if(compareAndSet(expected, new)) new else get()
}