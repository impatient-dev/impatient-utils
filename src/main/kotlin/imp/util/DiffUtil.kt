package imp.util

object DiffUtil {
	/**Finds which objects are in one collection but not the other, based on equals and hashCode.
	 * Multiple copies of 1 element are treated as just 1 element (the collections are treated as sets).*/
	fun <T> diff(old: Collection<T>, new: Collection<T>): AddedRemoved<T> {
		val added = HashSet(new)
		for(item in old)
			added.remove(item)
		val removed = HashSet(old)
		for(item in new)
			removed.remove(item)
		return AddedRemoved(added, removed)
	}


	/**Helps you compare maps. Unusually, this function treats a null value differently from a key/value entry that's just missing from the map.
	 * For each key that's in one map but not the other, onKeyMismatch is called.
	 * For each key that's in both maps (even if either or both values are null), calls onKeyMatch.
	 * "Added" is true if the key is missing from the old map and false if it's missing from the new map.*/
	fun <K,V> diffMapsHelperNull(old: Map<K,V>, new: Map<K,V>, onKeyMismatch: (key: K, added: Boolean) -> Unit, onKeyMatch: (key: K, oldVal: V?, newVal: V?) -> Unit) {
		for((key, oldVal) in old) {
			if(!new.containsKey(key))
				onKeyMismatch(key, false)
			else
				onKeyMatch(key, oldVal, new[key])
		}
		for((key, _) in new)
			if(!old.containsKey(key))
				onKeyMismatch(key, true)
	}
}


class AddedRemoved<T> (
	val added: Set<T>,
	val removed: Set<T>,
)


/**Detects differences between 2 maps.*/
inline fun <K: Any, V: Any> diff(
	old: Map<K,V>,
	new: Map<K,V>,
	/**Called for every key in new that's missing from old.*/
	onAdd: (K,V) -> Unit,
	/**Called for every key in old that's missing from new.*/
	onRemove: (K,V) -> Unit,
	/**Called for every key that's in both maps, where the values aren't equal. a is from old; b is from new.*/
	onModify: (K, a: V, b: V) -> Unit,
) {
	for((key, value) in old)
		if(!new.containsKey(key))
			onRemove(key, value)
	for((key, b) in new) {
		val a = old[key]
		if(a == null)
			onAdd(key, b)
		else if(a != b)
			onModify(key, a, b)
	}
}


/**Detects differences between 2 lists, where each list is sorted by the same unique key (K), lowest first.
 * This function will not work correctly if either list contains 2 items with the same key.*/
inline fun <V, K: Comparable<K>> diffSortedBy(
	old: Collection<V>,
	new: Collection<V>,
	key: (V) -> K,
	/**Called for every value in new that's missing from old.*/
	onAdd: (V) -> Unit,
	/**Called for every value in old that's missing from new.*/
	onRemove: (V) -> Unit,
	/**Called for every key that's in both maps, where the values aren't equal. a is from old; b is from new.*/
	onModify: (a: V, b: V) -> Unit,
) {
	if(old.isEmpty())
		for(item in new)
			onAdd(item)
	else if(new.isEmpty())
		for(item in old)
			onRemove(item)

	val ai = old.iterator()
	val bi = new.iterator()
	var a = ai.next()
	var b = bi.next()
	var ak = key(a)
	var bk = key(b)

	// Note: there is a block of code for a with 2 copies, and same for b; both have comments marking the start and end.
	// Each block advances the respective iterator, and ends the function if it's out of elements.
	// This mess exists because local inline functions aren't currently supported.

	while(true) {
		if(ak == bk) {
			if(a != b)
				onModify(a, b)
			// advance a
			if(!ai.hasNext()) {
				onAdd(b)
				bi.forEach { onAdd(it) }
				return
			}
			a = ai.next()
			ak = key(a)
			// end advance
			// advance b
			if(!bi.hasNext()) {
				onRemove(a)
				ai.forEach { onRemove(it) }
				return
			}
			b = bi.next()
			bk = key(b)
			// end advance
		} else if(ak < bk) {
			onRemove(a)
			// advance a
			if(!ai.hasNext()) {
				onAdd(b)
				bi.forEach { onAdd(it) }
				return
			}
			a = ai.next()
			ak = key(a)
			// end advance
		} else {
			onAdd(b)
			// advance b
			if(!bi.hasNext()) {
				onRemove(a)
				ai.forEach { onRemove(it) }
				return
			}
			b = bi.next()
			bk = key(b)
			// end advance
		}
	}
}