package imp.util


/**Finds the inverse probability. The initial probability should be in [0,1.]*/
fun Double.probInv(): Double = 1 - this

