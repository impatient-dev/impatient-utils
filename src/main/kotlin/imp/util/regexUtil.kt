package imp.util

import java.util.regex.Pattern

/**Returns true if the regex matches the entire string.*/
inline fun Pattern.impMatchesWhole(str: String): Boolean = this.matcher(str).matches()