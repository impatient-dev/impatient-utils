package imp.util

/**Does nothing if the key is null.*/
inline fun <K,V> MutableMap<K,V>.removeNonNull(key: K?): V? =
	if(key == null) null else remove(key)

/**Returns a copy of this map, with an entry added/replaced.*/
fun <K,V> Map<K,V>.withEntry(key: K, value: V): HashMap<K,V> {
	val out = HashMap<K,V>(size + 1)
	out.putAll(this)
	out[key] = value
	return out
}

/**Returns a copy of this map, with a key removed. If the key isn't present, this is just a copy.*/
fun <K,V> Map<K,V>.withoutKey(key: K): HashMap<K,V> {
	val out = HashMap<K,V>(this)
	out.remove(key)
	return out
}