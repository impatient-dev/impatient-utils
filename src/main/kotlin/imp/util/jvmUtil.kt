package imp.util

import kotlin.concurrent.thread


fun addShutdownHook(threadName: String, block: () -> Unit) {
	Runtime.getRuntime().addShutdownHook(thread(start = false, name = threadName, block = block))
}