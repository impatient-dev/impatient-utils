package imp.util


private fun fmtNumber(n: Long, unit: String, space: Boolean) = if(space) "$n $unit" else "${n}$unit"


private fun fmtNumber(number: Long, units: Array<Pair<Int, String>>, space: Boolean): String {
	var n = number / units[0].first
	var u = 0

	while(true) {
		if(u + 1 == units.size)
			return fmtNumber(n, units[u].second, space)
		val nn = n / units[u + 1].first
		if(nn < 10)
			return fmtNumber(n, units[u].second, space)
		n = nn
		u++
	}
}


private val byteUnits = arrayOf(
	Pair(1, "B"),
	Pair(1024, "KiB"),
	Pair(1024, "MiB"),
	Pair(1024, "GiB"),
	Pair(1024, "TiB")
)

/**Returns something like 123 MiB.*/
fun fmtBytes(bytes: Long, space: Boolean = true) = fmtNumber(bytes, byteUnits, space)
/**Returns something like 123 MiB/s.*/
fun fmtBytesPerSecond(bps: Long, space: Boolean = true) = fmtBytes(bps, space) + "/s"