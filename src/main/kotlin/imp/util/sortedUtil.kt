package imp.util


/*
Note on binary search: for insertion point i, it returns -i - 1 if the item isn't currently in the list.
The way to invert this transformation (in both directions) is to negate it and subtract 1.
There should be no + sign used when handling the output of binarySearch() (unless you do -(i+1)).
 */

fun <T: Comparable<T>> List<T>.isSorted(): Boolean {
	if(isEmpty())
		return true
	val it = iterator()
	var prev = it.next()
	while(it.hasNext()) {
		val current = it.next()
		if(current < prev)
			return false
		prev = current
	}
	return true
}

inline fun <T, U: Comparable<U>> List<T>.isSortedBy(transform: (T) -> U): Boolean {
	if(isEmpty())
		return true
	val it = iterator()
	var prev = transform(it.next())
	while(it.hasNext()) {
		val current = transform(it.next())
		if(current < prev)
			return false
		prev = current
	}
	return true
}



/**Adds an item to a sorted list.*/
inline fun <T : Comparable<T>> MutableList<T>.addSorted(item: T, allowReplace: Boolean = false) {
	val i = this.binarySearch { it.compareTo(item) }
	if(i >= 0) {
		if(!allowReplace)
			throw IllegalStateException("Duplicate at index $i for $item in list of size ${this.size}")
		this[i] = item
	} else {
		add(-i - 1, item)
	}
}

/**Adds an item to a list that's sorted by some attribute (U - lowest first).
 * If allowReplace is false, any existing item with the same sort value will cause an exception.*/
inline fun <T, U: Comparable<U>> MutableList<T>.addSortedBy(item: T, allowReplace: Boolean = false, crossinline transform: (T) -> U) {
	val u = transform(item)
	var i = this.binarySearch { transform(it).compareTo(u) }
	if(i >= 0) {
		if(!allowReplace)
			throw IllegalStateException("Duplicate at index $i for $u in list of size ${this.size}")
		this[i] = item
	} else {
		i = -i - 1
		this.add(i, item)
	}
}

/**Adds an element to the list using the provided comparison function and a binary search.
 * The comparison must return negative numbers for elements too early in the list, and positive numbers for elements too late.
 * In general, the comparison should probably look like { it.foo.compareTo(reference.foo) }
 * This function will replace an existing element if it finds one for which the comparison function returns 0;
 * otherwise, it will expand the list by 1 and add the new element at the appropriate position.*/
fun <T> MutableList<T>.addSortedWith(element: T, compare: (T) -> Int) {
	val i = this.binarySearch(comparison = compare)
	if(i >= 0)
		this.set(i, element)
	else
		this.add(-i -1, element)
}


/**Adds an element to the list, where the list is sorted by the provided comparison function.*/
fun <T> MutableList<T>.addSortedWith(element: T, allowReplace: Boolean = false, compare: (T,T) -> Int) {
	val i = this.binarySearch { compare(it, element) }
	if(i >= 0) {
		if(!allowReplace)
			throw IllegalStateException("Duplicate at index $i for $element in list of size ${this.size}")
		this.set(i, element)
	} else {
		this.add(-i - 1, element)
	}
}

/**Similar to addSortedBy, but creates a new list and leaves the original unchanged.*/
inline fun <T, C: Comparable<C>> List<T>.plusSortedBy(item: T, allowReplace: Boolean = false, crossinline transform: (T) -> C): ArrayList<T> {
	val out = ArrayList<T>(size + 1)
	out.addAll(this)
	out.addSortedBy(item, allowReplace, transform)
	return out
}

/**Similar to addSortedWith, but creates a new list and leaves the original unchanged.
 * @see addSortedWith*/
inline fun <T> List<T>.plusSortedWith(element: T, noinline compare: (T) -> Int): ArrayList<T> {
	val i = this.binarySearch(comparison = compare)
	return if(i >= 0) {
		val out = ArrayList(this)
		out.set(i, element)
		out
	} else {
		val out = ArrayList<T>(size + 1)
		out.addAll(this)
		out.add(-i - 1, element)
		out
	}
}

/**Similar to addSortedWith, but creates a new list and leaves the original unchanged.
 * @see addSortedWith*/
fun <T> List<T>.plusSortedWith(element: T, compare: (T,T) -> Int): ArrayList<T> {
	val i = this.binarySearch { compare(it, element) }
	return if(i >= 0) {
		val out = ArrayList(this)
		out.set(i, element)
		out
	} else {
		val out = ArrayList<T>(size + 1)
		out.addAll(this)
		out.add(-i - 1, element)
		out
	}
}


/**For a list that's sorted by some attribute (U - lowest first), finds the index of a particular U.
 * Returns a negative number if the U doesn't exist in this list.*/
inline fun <T, U: Comparable<U>> List<T>.idxSortedBy(search: U, crossinline transform: (T) -> U): Int = this.binarySearch { transform(it).compareTo(search) }
/**For a list that's sorted by some attribute (U - lowest first), returns the item containing the requested U, or null if no item does.*/
inline fun <T, U: Comparable<U>> List<T>.getSortedBy(search: U, crossinline transform: (T) -> U): T? {
	val i = this.idxSortedBy(search, transform)
	return if(i < 0) null else this[i]
}


/**For a sorted list, removes and returns some element matching the comparison, if any exists.
 * The comparison must return negative numbers for elements too early in the list, and positive numbers for elements too late.
 * In general, the comparison should probably look like { it.foo.compareTo(reference.foo) }*/
fun <T> MutableList<T>.removeSortedWith(compare: (T) -> Int): T? {
	val i = this.binarySearch(comparison = compare)
	if(i >= 0)
		return this.removeAt(i)
	return null
}

/**For a sorted list, removes and returns some element if it's in the list.
 * The list must be sorted by the provided comparison function.*/
fun <T> MutableList<T>.removeSortedWith(element: T, compare: (T, T) -> Int): T? {
	val i = this.binarySearch { compare(it, element) }
	if(i >= 0)
		return this.removeAt(i)
	return null
}

/**Removes an item from a list that's sorted by some attribute (U).
 * Returns whether anything was removed.*/
inline fun <T, U: Comparable<U>> MutableList<T>.removeSortedBy(item: T, crossinline transform: (T) -> U): Boolean {
	val u = transform(item)
	val i = this.binarySearch { transform(it).compareTo(u) }
	if(i >= 0) {
		this.removeAt(i)
		return true
	}
	return false
}