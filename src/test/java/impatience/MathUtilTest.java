package impatience;

import org.junit.Test;

import static org.junit.Assert.*;

import static impatience.MathUtil.*;

public class MathUtilTest
{
	@Test public void roundToMultiple_i_basic()
	{
		assertEquals(9, roundToMultiple(8, 3));
	}
	@Test public void roundToMultiple_i_big()
	{
		assertEquals(500, roundToMultiple(451, 100));
		assertEquals(12345000, roundToMultiple(12345432, 1000));
		assertEquals(40000000, roundToMultiple(36000000, 10000000));
	}
}