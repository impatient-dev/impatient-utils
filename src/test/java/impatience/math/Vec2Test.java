package impatience.math;

import org.junit.Test;

import static org.junit.Assert.*;

public class Vec2Test
{
	public static final double TOLERANCE = 0.000001;


	public static void assertClose(Vec2 expected, Vec2 result)
	{
		try
		{
			assertEquals(expected.x, result.x, TOLERANCE);
			assertEquals(expected.y, result.y, TOLERANCE);
		}
		catch(AssertionError e)
		{
			throw new AssertionError("Expected: " + expected.toString() + "\nActual:  " + result.toString());
		}
	}


	@Test public void angle()
	{
		assertEquals(0, new Vec2(1, 0).angle(), TOLERANCE);
		assertEquals(Math.PI / 2, new Vec2(0, 1).angle(), TOLERANCE);
		assertEquals(Math.toRadians(45), new Vec2(1, 1).angle(), TOLERANCE);
	}


	@Test public void rotated()
	{
		assertClose(new Vec2(0, 1), new Vec2(1, 0).rotated(Math.PI / 2));
	}
}