package impatience.math;

import org.junit.Test;

import static org.junit.Assert.*;

import static impatience.math.GeomUtil.*;

import static impatience.math.Vec2Test.*;

public class GeomUtilTest
{
	private static void assertEqual(Vec2 expected, Vec2 actual)
	{
		double magnitude = expected.magnitude();
		if(magnitude == 0)
			assertEqual(expected, actual, TOLERANCE);
		else
			assertEqual(expected, actual, magnitude / 1000 / 1000);
	}
	private static void assertEqual(Vec2 expected, Vec2 actual, double tolerance)
	{
		if(Math.abs(expected.x - actual.x) > tolerance || Math.abs(expected.y - actual.y) > tolerance)
			throw new AssertionError("\nExpected:\t" + expected.x + "\t" + expected.y + "\nActual:  \t" + actual.x + "\t" + actual.y);
	}



	@Test public void intersectionBasic()
	{
		Vec2 a = new Vec2(1, 1), b = new Vec2(1, -1);
		assertEqual(new Vec2(0, 0), intersectionLines(a, a, b, b));
		assertEqual(new Vec2(0, 0), intersectionLines(b, b, a, a));
	}
	@Test public void intersection2()
	{
		Vec2 ap = new Vec2(0, 2), bp = new Vec2(0, 4), as = new Vec2(1, -1), bs = new Vec2(1, -3);
		assertEqual(new Vec2(1, 1), intersectionLines(ap, as, bp, bs));
		assertEqual(new Vec2(1, 1), intersectionLines(bp, bs, ap, as));
	}
	/**An intersection problem that requires a lot of precision far from the origin.*/
	@Test public void intersectionFar()
	{
		Vec2 ap = new Vec2(170.0, 170.002), bp = new Vec2(170.000, 170.004), as = new Vec2(1, -1), bs = new Vec2(1, -3);
		assertEqual(new Vec2(170.001, 170.001), intersectionLines(ap, as, bp, bs), 0.0001);
		assertEqual(new Vec2(170.001, 170.001), intersectionLines(bp, bs, ap, as), 0.0001);
	}



	@Test public void paramProjection2Basic()
	{
		assertEquals(0, paramProjectOntoLine(new Vec2(-1,1), new Vec2(1,1)), TOLERANCE);
	}
	@Test public void projectionBasic()
	{
		assertClose(new Vec2(0, 0), projectionOntoLine(new Vec2(-1,1), new Vec2(-5,-5), new Vec2(5,5)));
	}
	@Test public void paramProjectionBasic()
	{
		assertEquals(0, paramProjectOntoLine(new Vec2(-1,1), new Vec2(0,0), new Vec2(5,5)), TOLERANCE);
	}
}