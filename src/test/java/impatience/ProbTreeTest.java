package impatience;

import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.*;

public class ProbTreeTest
{
	
	@Test public void testPickRand() throws Exception
	{
		ProbTree<Integer> tree = new ProbTree<>();
		tree.add(0, 1);
		tree.add(1, 2);
		tree.add(2, 3);
		tree.add(3, 4);
		float[] expected = {.1f, .2f, .3f, .4f};

		int[] freqs = new int[4];
		Random rand = new Random(12345);
		int times = 1000 * 1000;
		for(int c = 0; c < times; c++)
			freqs[tree.pickRand(rand)]++;

		float[] actual = new float[expected.length];
		boolean same = true;
		for(int c = 0; c < actual.length; c++)
		{
			actual[c] = freqs[c] / (float) times;
			if(Math.abs(actual[c] - expected[c]) > 0.01)
				same = false;
		}

		if(!same)
		{
			System.err.println("frequency comparison");
			for(int c = 0; c < expected.length; c++)
				System.err.println((c+1) + " expected " + expected[c] + ", got " + actual[c]);
			fail();
		}
	}

	/**Tests that the tree will never pick something with a frequency of 0.*/
	@Test public void testPickRandWith0s() throws Exception
	{
		ProbTree<Integer> tree = new ProbTree<>();
		tree.add(0, 1);
		tree.add(1, 2);
		tree.add(2, 0);
		tree.add(3, 7);
		float[] expected = {.1f, .2f, 0f, .7f};

		int[] freqs = new int[4];
		Random rand = new Random(12345);
		int times = 1000 * 1000;
		for(int c = 0; c < times; c++)
			freqs[tree.pickRand(rand)]++;

		float[] actual = new float[expected.length];
		boolean same = true;
		for(int c = 0; c < actual.length; c++)
		{
			actual[c] = freqs[c] / (float) times;
			if(Math.abs(actual[c] - expected[c]) > 0.01)
				same = false;
		}

		if(!same)
		{
			System.err.println("frequency comparison");
			for(int c = 0; c < expected.length; c++)
				System.err.println((c+1) + " expected " + expected[c] + ", got " + actual[c]);
			fail();
		}

		assertEquals(0, freqs[2]);
	}
}