package impatience;

import org.junit.Test;

import static org.junit.Assert.*;

public class TextUtilTest
{
	@Test public void join()
	{
		assertEquals("a-b-c", TextUtil.join(new String[]{"a", "b", "c"}, "-"));
	}
}