package imp.util

import org.junit.Assert.assertArrayEquals
import org.junit.Assert.assertEquals
import org.junit.Test

class CollectionTest {
	@Test fun testArrayPlus_start() = assertArrayEquals(arrayOf(0, 1, 2, 3), arrayOf(1, 2, 3).plusAt(0, 0))
	@Test fun testArrayPlus_mid1() = assertArrayEquals(arrayOf(0, 1, 2, 3), arrayOf(0, 2, 3).plusAt(1, 1))
	@Test fun testArrayPlus_mid2() = assertArrayEquals(arrayOf(0, 1, 2, 3), arrayOf(0, 1, 3).plusAt(2, 2))
	@Test fun testArrayPlus_end() = assertArrayEquals(arrayOf(0, 1, 2, 3), arrayOf(0, 1, 2).plusAt(3, 3))

	@Test fun testArrayMinus_start() = assertArrayEquals(arrayOf(1, 2), arrayOf(0, 1, 2).minusAt(0))
	@Test fun testArrayMinus_mid() = assertArrayEquals(arrayOf(0, 2), arrayOf(0, 1, 2).minusAt(1))
	@Test fun testArrayMinus_end() = assertArrayEquals(arrayOf(0, 1), arrayOf(0, 1, 2).minusAt(2))

	@Test fun testListMove_front_back() = assertEquals(listOf(2, 3, 1), mutableListOf(1, 2, 3).also { it.move(0, 2) })
	@Test fun testListMove_back_front() = assertEquals(listOf(3, 1, 2), mutableListOf(1, 2, 3).also { it.move(2, 0) })
}