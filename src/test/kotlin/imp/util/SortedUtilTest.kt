package imp.util

import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test

class SortedUtilTest {
	@Test fun test_sortedAdd() {
		testAddSortedBy("")

		testAddSortedBy("1", "2")
		testAddSortedBy("2", "1")

		testAddSortedBy("1", "2", "3")
		testAddSortedBy("1", "3", "2")
		testAddSortedBy("2", "1", "3")
		testAddSortedBy("2", "3", "1")
		testAddSortedBy("3", "1", "2")
		testAddSortedBy("3", "2", "1")
	}


	@Test fun test_sortedIdx_empty() {
		val i = emptyList<StringHolder>().idxSortedBy("") { it.str }
		assertTrue(i < 0)
	}

	@Test fun test_sortedIdx() {
		val list = ArrayList<String>()
		list.add("")
		testIdxSortedBy(0, "", list)

		list.add("1")
		testIdxSortedBy(0, "", list)
		testIdxSortedBy(1, "1", list)

		list.add("2")
		testIdxSortedBy(0, "", list)
		testIdxSortedBy(1, "1", list)
		testIdxSortedBy(2, "2", list)

		list.add("3")
		testIdxSortedBy(0, "", list)
		testIdxSortedBy(1, "1", list)
		testIdxSortedBy(2, "2", list)
		testIdxSortedBy(3, "3", list)
	}
}


/**Ensures addSortedBy ends up with a sorted list.*/
private fun testAddSortedBy(vararg strings: String) {
	val list = ArrayList<StringHolder>(strings.size)
	for(str in strings)
		list.addSortedBy(StringHolder(str)) { it.str }
	val mapped = list.map { it.str }
	val sorted = mapped.sorted()
	assertEquals(mapped, sorted)
}


private fun testIdxSortedBy(expectIdx: Int, search: String, strings: List<String>) {
	val list = strings.map { StringHolder(it) }
	val result = list.idxSortedBy(search) { it.str }
	assertEquals(expectIdx, result)
}

private class StringHolder (
	val str: String,
)