package imp.util

import org.junit.Assert.assertEquals
import org.junit.Test

class StrUtilTest {
	@Test fun test_splitByWhitespace_empty() = assertEquals(emptyList<String>(), "".splitByWhitespace())
	@Test fun test_splitByWhitespace_1() = assertEquals(listOf("1"), "1".splitByWhitespace())
	@Test fun test_splitByWhitespace_1a() = assertEquals(listOf("1"), " 1".splitByWhitespace())
	@Test fun test_splitByWhitespace_1z() = assertEquals(listOf("1"), "1 ".splitByWhitespace())
	@Test fun test_splitByWhitespace_1b() = assertEquals(listOf("1"), " 1 ".splitByWhitespace())
	@Test fun test_splitByWhitespace_word() = assertEquals(listOf("abc"), "abc".splitByWhitespace())
	@Test fun test_splitByWhitespace_numbers() = assertEquals(listOf("1", "2"), "1 2".splitByWhitespace())
	@Test fun test_splitByWhitespace_complex() = assertEquals(listOf("aaa", "bb"), "   aaa   bb  ".splitByWhitespace())
}