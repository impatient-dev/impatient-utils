package imp.util

import org.junit.Assert.assertEquals
import org.junit.Test

class ListUtilTest {
	@Test fun test_lastIndexFromLast() {
		val list = listOf(0, 1, 2, 3)
		assertEquals(2, list.lastIndexFromLast { it >= 2 })
	}

	@Test fun test_cutDownToSize() {
		val list = arrayListOf(0, 1, 2, 3)
		list.cutDownToSize(2)
		assertEquals(listOf(0, 1), list)
	}

	@Test fun test_removeEverythingAfter() {
		val list = arrayListOf(0, 1, 2, 3)
		list.removeEverythingAfter(1)
		assertEquals(listOf(0, 1), list)
	}
}