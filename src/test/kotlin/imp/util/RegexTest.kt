package imp.util

import org.junit.Assert.assertEquals
import org.junit.Test
import java.util.regex.Pattern

class RegexTest {
	val letters = Pattern.compile("[a-z]*")
	@Test fun test_0() = assertEquals(true, letters.impMatchesWhole(""))
	@Test fun test_1() = assertEquals(true, letters.impMatchesWhole("a"))
	@Test fun test_2() = assertEquals(true, letters.impMatchesWhole("abc"))
	@Test fun test_start() = assertEquals(false, letters.impMatchesWhole("1a"))
	@Test fun test_end() = assertEquals(false, letters.impMatchesWhole("a1"))
}