package imp.util.fmt

import imp.util.withDate
import imp.util.withTime
import org.junit.Assert.assertEquals
import org.junit.Test
import java.time.LocalDateTime

private val now = LocalDateTime.of(2020, 6, 15, 12, 30) // Monday

class ParseDateTimeTest {
	@Test fun test_empty() = test(now, "", null, now)

	@Test fun test_yesterday() = test(now, "yesterday", null, now.minusDays(1))
	@Test fun test_tomorrow() = test(now, "tomorrow", null, now.plusDays(1))

	@Test fun test_3_30() = test(now, "3:30", null, now.withTime(3, 30))
	@Test fun test_3_30_am() = test(now, "3:30 AM", null, now.withTime(3, 30))
	@Test fun test_3_30_pm() = test(now, "3:30 p.m.", null, now.withTime(3+12, 30))

	@Test fun test_12a() = test(now, "12a", null, now.withTime(0, 0))
	@Test fun test_12pm() = test(now, "12 PM", null, now.withTime(12, 0))

	@Test fun test_tuesday() = test(now, "Tuesday", null, now.withDayOfMonth(16))
	@Test fun test_tuesday_future() = test(now, "Tuesday", true, now.withDayOfMonth(16))
	@Test fun test_tuesday_past() = test(now, "Tuesday", false, now.withDayOfMonth(9))
	@Test fun test_sun() = test(now, "sun", null, now.withDayOfMonth(14))
	@Test fun test_sun_future() = test(now, "sun", true, now.withDayOfMonth(21))
	@Test fun test_sun_past() = test(now, "sun", false, now.withDayOfMonth(14))

	@Test fun test_YMD() = test(now, "2023-04-05", null, now.withDate(2023, 4, 5))
	@Test fun test_YMD_8a() = test(now, "2023-04-05 8a", null, LocalDateTime.of(2023, 4, 5, 8, 0))
	@Test fun test_MD() = test(now, "04-05", null, now.withMonth(4).withDayOfMonth(5))



	private fun test(now: LocalDateTime, str: String, future: Boolean?, expected: LocalDateTime) {
		val parsed = parseDateTime(str, now, future)
		assertEquals(expected, parsed)
	}
}