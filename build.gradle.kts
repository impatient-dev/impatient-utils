plugins {
	id("org.jetbrains.kotlin.jvm")
}

group = "imp-dev"
version = "0.0.0"
val kotlin_version: String by project

repositories {
	mavenCentral()
}

dependencies {
	api("org.jetbrains.kotlin:kotlin-stdlib:$kotlin_version")
	// logback-android doesn't support SLF4J 2 - https://github.com/tony19/logback-android
	api("org.slf4j:slf4j-api:1.7.32")
	api("org.jetbrains.kotlin:kotlin-reflect:$kotlin_version")

	api("commons-io:commons-io:2.16.1")
	api("com.google.code.findbugs:jsr305:3.0.1") // Java @Nullable

	// don't use imp-junit4 to avoid a circular dependency
	testImplementation("junit:junit:4.12")
}