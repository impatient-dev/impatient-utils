# impatient-utils
This is a Java/Kotlin util project containing various useful things.
This library should be kept small, with minimal dependencies.

Code inside may use the SLF4J API unless otherwise specified;
users of this library should use logback-classic or a bridge to send the logging output somewhere.